clear all; clc; close all; rng(1)
camera=webcam(1);
preview(camera)
pause
[eyeRect,referenceImage] = alignEyes(camera);
eyeRect=[0,0,640,480];
%referenceImage=displayRect(camera,eyeRect);
% Parameters
xmin = 0;
xmax = 190;
ymin = 0;
ymax = 100;
xnum = 20;
ynum = 10;
maxTrainingNum = xnum*ynum;
xsamp = linspace(xmin,xmax,xnum);
ysamp = linspace(ymin,ymax,ynum);
[xsamp_mesh,ysamp_mesh] = meshgrid(xsamp,ysamp);
xy_ind = randperm(maxTrainingNum);
genY = @() randi([ymin,ymax],1);
genX = @() randi([xmin,xmax],1);
markerStyle = 'ro';
markerSize = 18;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Setup Figure
f = figure;
set(f,'numbertitle','off')
%set(gcf,'units','normalized','outerposition',[-0.8694,-0.14,0.8694,1])
set(gcf,'units','normalized','outerposition',[0,0,1,1])
set(gca,'xticklabel',[],'yticklabel',[])
axes('position',[0,0,1,1])
daspect([1,1,1])
xlim([xmin-2,xmax+2])
ylim([ymin-2,ymax+2])
text((xmax-xmin)/2,(ymax-ymin)/2,'PRESS SPACE TO BEGIN','fontsize',45,...
    'horizontalalignment','center')
pause

% Loop through different training Points
counter = 1;
locations = [];
img_cell = cell(1,maxTrainingNum);
while counter <= maxTrainingNum
    x = xsamp_mesh(xy_ind(counter));%genX();
    y = ysamp_mesh(xy_ind(counter));%genY();
    locations = [locations;x,y];
    if isinf(maxTrainingNum)
        set(f,'name',sprintf('INF-MODE     frame %d     p = (%d,%d)',...
                            counter,x,y))
    else
        set(f,'name',sprintf('frame %d/%d     p = (%d,%d)',...
                            counter,maxTrainingNum,x,y))
    end
    plot(x,y,markerStyle,'markersize',markerSize,'markerfacecolor',markerStyle(1))
    text(x,y,num2str(counter),'horizontalalignment','center')
    daspect([1,1,1])
    xlim([xmin-2,xmax+2])
    ylim([ymin-2,ymax+2])
    
    pause
    % Call to take image
    img_cell{counter} = takePic(camera,eyeRect);
    counter = counter + 1;
end
timestampfull = int16(clock);
timestamp = sprintf('%d_',timestampfull);
timestamp(end)=[];
todaystring = sprintf('%d_%d_%d',timestampfull(1),timestampfull(2),timestampfull(3));
% Check whether or not today's directory exists
if ~isdir(fullfile('data',todaystring))
    mkdir('data',todaystring)
end

save(fullfile('data',todaystring,sprintf('data_%s.mat',timestamp)),'img_cell','locations','maxTrainingNum','xmin','xmax','ymin','ymax','referenceImage')
clear('camera')
close all;
return 
%%
for i = 1:maxTrainingNum
    imshow(img_cell{i})
    pause(0.05)
end
%%
dset1 = load('data\2015_11_24\data_2015_11_24_21_58_6.mat');
dset2 = load('data\2015_11_24\data_2015_11_24_22_0_39.mat');
for i = [1:length(dset1.img_cell)]
    %figure
    subplot(131); imshow(dset1.img_cell{i})
    subplot(132); imshow(dset2.img_cell{i})
    subplot(133); imshow(abs(dset1.img_cell{i}-dset2.img_cell{i}))
    pause(0.01)
end