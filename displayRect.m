function image =displayRect(camera,rect)

image=snapshot(camera);
h=figure;
imagesc(image);
rectangle('position',rect,'EdgeColor','b')
pause
close(h)
end