function [Xdir,possibleDirections,directionMappings] = calculateDirections(data,fixationRadius,nTheta,display)
if nargin < 4,display = false;,end
dTheta = 2*pi/nTheta;
directions = (0:nTheta-1)*dTheta;
if display,plotDirections(directions,fixationRadius);,end
Xdir = zeros(size(data,1),1);
for i = 2:size(data,1)
    currentV = data(i,:) - data(i-1,:);
    if norm(currentV) < fixationRadius
        Xdir(i) = length(directions) + 1; % user is fixated
        continue
    end
    ang = mod(atan2(currentV(2),currentV(1)),2*pi);
    [~,Xdir(i)] = min(abs(ang-directions));
end
possibleDirections = 1:length(directions)+1;
directionMappings = [directions,-1];
end

function plotDirections(dir,fixationRadius)
fontsize = 15;
drawArrow = @(x,y) quiver( x(1),y(1),x(2)-x(1),y(2)-y(1),0,'maxheadsize',1);
%figure;
t = linspace(0,2*pi,100);
if fixationRadius ~= 0
    plot(fixationRadius*cos(t),fixationRadius*sin(t));
    fixRad = fixationRadius;
else
    fixRad = 1;
end 
hold on;
text(0,0,sprintf('%d',length(dir)+1),'fontsize',fontsize,'horizontalalignment','center'); hold on;
for i = 1:length(dir)
    if fixationRadius ~= 0
        [x1,y1] = pol2cart(dir(i),fixRad);
    else
        x1 = 0; 
        y1 = 0;
    end
    [x2,y2] = pol2cart(dir(i),2*fixRad);
    drawArrow([x1,x2],[y1,y2]);
    [x3,y3] = pol2cart(dir(i),2.5*fixRad);
    text(x3,y3,sprintf('%d',i),'fontsize',fontsize,'horizontalalignment','center');
end
daspect([1,1,1]);
xlim([-1,1]*3.5*fixRad); ylim([-1,1]*3.5*fixRad)
end