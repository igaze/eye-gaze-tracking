function [angleTrainData, possibleStates, stateMappings,directionMappings] = extendState(data,...
                                                                      fixationRadius,...
                                                                      nTheta,...
                                                                      numStatesToGroup,...
                                                                      display)
if nargin < 5,display = false;,end
% possibleDirections = 1:numDirections + 1 for fixation
% directionMappings(i) = the angle possibleDirections(i) refers to
%   directionMappings(numDirections + 1) = -1 by default, meaning fixation state
[Xdir,possibleDirections,directionMappings] = calculateDirections(data,fixationRadius,nTheta,display);
% stateMappings(i,:) = [Ind_Of_Theta_n,Ind_Of_Theta_{n-1}] contains indices
%   referred from "possibleDirections", not values
stateMappings = permn(possibleDirections,numStatesToGroup);
possibleStates = (1:size(stateMappings,1))';
angleTrainData = zeros(size(data,1),1);
for i = 1+numStatesToGroup:length(Xdir) % must ignore first, and start after numStatesToGroup
    state = zeros(1,numStatesToGroup);
    for j = 1:numStatesToGroup
        state(j) = Xdir(i+1-j);
    end
    angleTrainData(i) = closest(state,stateMappings);
end

end

function index = closest(row,mat)
for i = 1:size(mat,1)
    if isequal(mat(i,:),row)
        index = i;
        return
    end
end
end

