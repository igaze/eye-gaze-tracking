clear all; clc; close all;
load ../eyeDataBaseline;
fontsize = 15;
%% Constants
fixationRadius = 1;
numStatesToGroup = 1;
pxVar = 5^2;
display = false;
plotNum = 500;
nTheta = [4,6,8,12,16,32,64,128];


dirToPlot = [4,8,16];
%% angleHMM
trainError = zeros(numDataSets,length(nTheta));
testError = zeros(numDataSets,length(nTheta));
for ti = 1:length(nTheta)
    gaussx = -((nTheta(ti)+1)^numStatesToGroup-1)/2:((nTheta(ti)+1)^numStatesToGroup-1)/2-1;
    gauss =  1/sqrt(2*pi*pxVar)*exp(-(gaussx.^2)/(2*pxVar));
    halfgauss = (gauss/(2*sum(gauss)))';
    halfgauss = circshift(halfgauss,-((nTheta(ti)+1)^numStatesToGroup-1)/2);
    for dInd = 1:numDataSets
        n = dataSetNumFrames(dInd);
        startInd = 1+numStatesToGroup;
        if dInd == 1 && ~isempty(dirToPlot) && (nTheta(ti) == dirToPlot(1))
            display = true;
            subplot(1,3,3-length(dirToPlot)+1);
            dirToPlot(1) = [];
        end
        [angleTrainData, possibleStates, stateMappings,directionMappings] = extendState(data{dInd}.train,...
                                                                          fixationRadius,...
                                                                          nTheta(ti),...
                                                                          numStatesToGroup,display);
        display = false;
        [angleTestData,~,~,~] = extendState(data{dInd}.test,...
                                               fixationRadius,...
                                               nTheta(ti),...
                                               numStatesToGroup);
        [tr,em] = hmmestimate(angleTrainData(startInd:end),angleTrainData(startInd:end),...
                              'Pseudoemissions',eye((nTheta(ti)+1)^numStatesToGroup),...
                              'Pseudotrans',eye((nTheta(ti)+1)^numStatesToGroup));
        % em generation
        delta = (nTheta(ti)+1)^(numStatesToGroup-1);
        for emi = 1:delta-1
            em(1+(emi-1)*delta:emi*delta,:) = repmat([circshift(halfgauss,(emi-1)*delta)',0.5],delta,1);
        end
        em(end-delta+1:end,:) = repmat(ones(1,size(em,2))/size(em,2),delta,1);
        %em = ones(size(em))/size(em,1);
        probTrainEstimate = hmmdecode(angleTrainData(startInd:end)',tr,em);
        [~,trainEstimate] = max(probTrainEstimate);
        probTestEstimate = hmmdecode(angleTestData(startInd:end)',tr,em);
        [~,testEstimate] = max(probTestEstimate);
        trainError(dInd,ti) = sum(angleTrainData(startInd:end) ~= trainEstimate')/numel(trainEstimate);
        testError(dInd,ti) = sum(angleTestData(startInd:end) ~= testEstimate')/numel(testEstimate);
        %figure; 
%         clf;subplot(211)
%         plot(angleTrainData(2:plotNum+1),'b*-'); hold on;
%         plot(trainEstimate(1:plotNum),'ro--');title('train')
%         subplot(212);
%         plot(angleTestData(2:plotNum+1),'b*-'); hold on;
%         plot(testEstimate(1:plotNum),'ro--');title('test')
        fprintf('dInd = %d, trainErr = %0.3f, testErr = %0.3f\n',dInd,trainError(dInd),testError(dInd))
    end
end
%%
output = [];
rowLabels = {};
for i = 1:length(nTheta)
    output = [output;trainError(:,i)';testError(:,i)'];
    rowLabels{end+1} = sprintf('$(%d,1)$',nTheta(i));
    rowLabels{end+1} = [];
end
matrix2latex(output, ...
    'out.tex', 'rowLabels',rowLabels,'columnLabels', dataSetNames, ...
    'alignment', 'c', 'format', '%-6.3f');
% printInd = 1;
% matrix2latex([trainError(:,printInd),testError(:,printInd)], ...
%     'out.tex', 'rowLabels',dataSetNames,'columnLabels', {'train','test'}, ...
%     'alignment', 'c', 'format', '%-6.2f');


figure; plot(nTheta,mean(testError),'b*-');
xlabel('N_\theta','fontsize',fontsize); 
ylabel('Average Success Rate','fontsize',fontsize);
set(gca,'fontsize',fontsize)
print -depsc avgTestErrorAcrossAllDataSets

% figure;
% for i = 1:12
% plot(nTheta,testError(i,:),'*-','color',rand(3,1)); hold on;
% xlabel('# \theta Divisions','fontsize',fontsize); 
% ylabel('Test Error','fontsize',fontsize);
% set(gca,'fontsize',fontsize)
% end
% print -depsc avgTestErrorAcrossAllDataSets

%%
