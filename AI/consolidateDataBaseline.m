% Place this in root folder of dataset
%
%   Contents:
%   
%       -data: all data (usage below)
%           data{dataSetNum}{numPerson}{numTrial}(xdim,ydim)
%       -dataSetNames: name of each dataSet
%       -numDataSets
%       -numPeople
%       -xlimits 
%       -ylimits
%
clear all; clc; close all;
%% Constants
numPeopleInTrainingSet = 12;
%%

fileNames = dir(fullfile('CSV','*-Screen.csv'));
data = cell(1,length(fileNames));
dataSetNames = cell(1,length(fileNames));
dataSetNumFrames = zeros(1,length(fileNames));
format = repmat('%f ',1,15*2*2);
format(end) = [];
for i = 1:length(fileNames)
    currentFile = fileNames(i).name;
    fid = fopen(fullfile('CSV',currentFile), 'rt');
    currentData = textscan(fid,format, 'headerlines', 2, 'delimiter', ',');
    temp = {};
    ind = 1;
    train = [];
    test = [];
    for j = 1:4:length(currentData)
        if ind <= numPeopleInTrainingSet
            train = [train;[currentData{j},currentData{j+1}];...
                           [currentData{j+2},currentData{j+3}]];
        else
            test = [test;[currentData{j},currentData{j+1}];...
                         [currentData{j+2},currentData{j+3}]];
        end
        ind = ind + 1;
    end
    %currentCrossValStruct = struct('train',train,...
                                  % 'test',test,...
                                   %'numFrames',size(currentData{1},1));
    data{i} = struct('train',train,...
                     'test',test,...
                     'numFrames',size(currentData{1},1));
    dataSetNames{i} = currentFile(1:end-11);
    dataSetNumFrames(i) = size(currentData{1},1);
    fclose(fid);
end

xlimits = [0,352];
ylimits = [0,288];
numDataSets = length(data);
numPeople = 15;

clear ans currentData currentFile fid fileNames format i j temp
save('eyeDataBaseline.mat')