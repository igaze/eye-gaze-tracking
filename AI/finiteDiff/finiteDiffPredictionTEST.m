clear all; clc; close all;
load ../eyeData;
order = 2;
fontsize = 15;
pxPerOneDeg = 16;
%x = [-10:1:10]';
%data = {{{[x,x.^2],[x,x.^2+3*rand(size(x))]}}};
%data = {{{[x,cos(x)],[x,cos(x)+rand(size(x))]}}};
%% Naive Backward Difference Propagation (velocity only)
errors = cell(1,numDataSets);
avgDataSetError = zeros(numDataSets,order);
ordNames = {'1st Ord.','2nd Ord.','3rd Ord.','4th Ord.','5th Ord.'};
ordPlot = {'r-o','b*-','m^-','bs-','gh-'};
pData = struct('err',{cell(1,order)},'p',{cell(1,order)},'speed',[]);
for ordInd = 1:order
    for dInd = 1:numDataSets
        errors{dInd} = zeros(numPeople,2);
        n = size(data{dInd}{1}{1},1);
        for pInd = 1:numPeople
            p1 = data{dInd}{pInd}{1};
            p_tilde1 = finiteDiffPrediction(data{dInd}{pInd}{1},ordInd);
            p2 = data{dInd}{pInd}{2};
            p_tilde2 = finiteDiffPrediction(data{dInd}{pInd}{2},ordInd);
            avgErr1 = 0;
            avgErr2 = 0;
            errVec1 = zeros(n,1);
            for i = 1:n
                errVec1(i) = norm(p1(i,:)-p_tilde1(i,:));
                avgErr1 = avgErr1 + norm(p1(i,:)-p_tilde1(i,:))/n;
                avgErr2 = avgErr2 + norm(p2(i,:)-p_tilde2(i,:))/n;
            end
            errors{dInd}(pInd,:) = [avgErr1,avgErr2];
            % For plotting
            if dInd == 1 && pInd == 1
                pData.err{ordInd} = errVec1;
                pData.p{ordInd} = p1;
                if ordInd == 1
                    speed = zeros(size(p1,1)-1,1);
                    for m = 1:numel(speed)
                        speed(m) = norm(p1(m+1,:) - p1(m,:));
                    end
                    pData.speed = speed;
                end
            end
        end
        avgDataSetError(dInd,ordInd) = mean(errors{dInd}(:));
    end
end
%%
matrix2latex(avgDataSetError'/pxPerOneDeg, 'out.tex', 'columnLabels', dataSetNames, 'rowLabels', ordNames(1:order), 'alignment', 'c', 'format', '%-6.3f');

figure;
subplot(211);
handles = [];
for i = 1:order
    handles(end+1) = plot(pData.err{i},ordPlot{i}); hold on;
end
xlabel('Frame #','fontsize',fontsize);
ylabel('Euclidean Error (in px)','fontsize',fontsize);
legend(handles,ordNames(1:order),'fontsize',fontsize);
title('Error in Finite Difference Prediction','fontsize',fontsize);

subplot(212);
plot(2:size(pData.speed,1)+1,pData.speed,'k*-')
xlim([1,size(pData.err{1},1)])
xlabel('Frame #','fontsize',fontsize);
ylabel('Speed (in px)','fontsize',fontsize);
title('Speed Plot','fontsize',fontsize);

% figure; subplot(211); plot(p1(:,1),p1(:,2),'ro-',p_tilde1(:,1),p_tilde1(:,2),'b*-');%xlim(xlimits);ylim(ylimits)
% legend('orig train','train estimate'); title(['Late Fusion (calculate x and y together): avg err = ',num2str(avgErr1)])
% subplot(212); plot(p2(:,1),p2(:,2),'ro-',p_tilde2(:,1),p_tilde2(:,2),'b*-');%xlim(xlimits);ylim(ylimits)
% legend('orig testing','test estimate');
% title(['Late Fusion (calculate x and y together): avg err = ',num2str(avgErr2)])
% return

% for i = 1:n
% subplot(211); plot(p1(1:i,1),p1(1:i,2),'ro-',p_tilde1(1:i,1),p_tilde1(1:i,2),'b*-');xlim(xlimits);ylim(ylimits)
% legend('orig train','train estimate'); title(['Late Fusion (calculate x and y together): avg err = ',num2str(avgErr1)])
% subplot(212); plot(p2(1:i,1),p2(1:i,2),'ro-',p_tilde2(1:i,1),p_tilde2(1:i,2),'b*-');xlim(xlimits);ylim(ylimits)
% legend('orig testing','test estimate');
% title(['Late Fusion (calculate x and y together): avg err = ',num2str(avgErr2)])
% pause
% end