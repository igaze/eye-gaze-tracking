% Naive Backward Difference Propagation
% 
%   -Data should be [x,y] where x and y are both column vectors
%   -Will load first "order+1" rows into Y to ensure data and Y are the
%       same size
%
function Y = finiteDiffPrediction(data,order)
n = size(data,1);
lookback = order + 1;
Y = data(1:lookback,:);
for i = lookback+1:n
    Y(end+1,:) = data(i-1,:);
    for j = 1:order
        % flip b/c top most data is oldest in data but finiteDiff needs top
        % to be most recent
        Y(end,:) = Y(end,:) + finiteDiff(flipud(data(i-1-j:i-1,:))); 
    end
end
end

function result = finiteDiff(x)
n = size(x,1);
if n <= 1
    error('x too small. len(x) > 1')
end
result = 0;
for i = 1:n
    result = result + (-1)^(i-1)*nchoosek(n-1,i-1)*x(i,:);
end
end