% Place this in root folder of dataset
%
%   Contents:
%   
%       -data: all data (usage below)
%           data{dataSetNum}{numPerson}{numTrial}(xdim,ydim)
%       -dataSetNames: name of each dataSet
%       -numDataSets
%       -numPeople
%       -xlimits 
%       -ylimits
%
clear all; clc; close all;

fileNames = dir(fullfile('CSV','*-Screen.csv'));
data = cell(1,length(fileNames));
dataSetNames = cell(1,length(fileNames));
format = repmat('%f ',1,15*2*2);
format(end) = [];
for i = 1:length(fileNames)
    currentFile = fileNames(i).name;
    fid = fopen(fullfile('CSV',currentFile), 'rt');
    currentData = textscan(fid,format, 'headerlines', 2, 'delimiter', ',');
    temp = {};
    for j = 1:4:length(currentData)
        temp{end+1} = {[currentData{j},currentData{j+1}],...
                       [currentData{j+2},currentData{j+3}]};
    end
    data{i} = temp;
    dataSetNames{i} = currentFile(1:end-11);
    fclose(fid);
end

xlimits = [0,352];
ylimits = [0,288];
numDataSets = length(data);
numPeople = length(data{1});

clear ans currentData currentFile fid fileNames format i j temp
save('eyeData.mat')