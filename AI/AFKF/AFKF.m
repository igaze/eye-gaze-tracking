function [predictedState, updatedModel] = AFKF(m,z)
%state = struct('x',[],'A',[1,deltat;0,1],'P',zeros(2),'K',zeros(2),'Q',diag([deltaW,deltaW]),'B',0,'u',0);
%% Predict
x_pred = m.A*m.x+m.B*m.u;
P_pred = m.A*m.P*m.A + m.Q;
predictedState = x_pred;

%% Update
K_next = P_pred*m.H'*inv(m.H*P_pred*m.H' + m.R);
x_next = x_pred + K_next*(z-m.H*x_pred);
P_next = (eye(size(P_pred))-K_next*m.H)*P_pred;
updatedModel = m;
updatedModel.K = K_next;
updatedModel.x = x_next;
updatedModel.P = P_next;