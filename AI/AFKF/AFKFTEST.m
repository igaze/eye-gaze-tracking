clear all; clc; close all;
load ../eyeData;
fontsize = 15;
%% Constants
pxPerOneDeg = 16;
pxPerDeg = 16;
deltat = 1/30;
deltaW = (pxPerDeg)^2; % b/c 1deg = 32px for this dataset
eyeTrackMeanSTD = 0.45*pxPerDeg;
Rk = eyeTrackMeanSTD^2;
model = struct('x',[],...
               'H',[1,0;0,1],...
               'A',[1,deltat;0,1],...
               'P',zeros(2),...
               'R',Rk,...
               'K',zeros(2),...
               'Q',diag([deltaW,deltaW]),...
               'B',0,...
               'u',0);

%% AFKF
errors = cell(1,numDataSets);
avgDataSetError = zeros(numDataSets,1);
ordNames = {'1st Ord.','2nd Ord.','3rd Ord.','4th Ord.','5th Ord.'};
ordPlot = {'r-o','b*-','m^-','bs-','gh-'};
pData = struct('err',[],'p',[],'speed',[]);
for dInd = 1:numDataSets
    errors{dInd} = zeros(numPeople,2);
    n = size(data{dInd}{1}{1},1);
    for pInd = 1:numPeople
        p1 = data{dInd}{pInd}{1};
        p2 = data{dInd}{pInd}{2};
        p_tilde1 = zeros(n,2);
        p_tilde2 = zeros(n,2);
        p_tilde1(1:2,:) = p1(1:2,:);
        p_tilde2(1:2,:) = p2(1:2,:);
        modelx1 = model;
        modely1 = model;
        modelx1.x = [p1(2,1);(p1(2,1)-p2(1,1))/deltat];
        modely1.x = [p1(2,2);(p1(2,2)-p2(1,2))/deltat];
        %
        modelx2 = model;
        modely2 = model;
        modelx2.x = [p1(2,1);(p1(2,1)-p2(1,1))/deltat];
        modely2.x = [p1(2,2);(p1(2,2)-p2(1,2))/deltat];
        if dInd == 1 && pInd == 1
            speed_pred = zeros(size(p1,1)-2,1);
        end
        for t = 3:n
            [sx1,modelx1] = AFKF(modelx1,[p1(t,1);(p1(t,1)-p1(t-1,1))/deltat]);
            [sy1,modely1] = AFKF(modely1,[p1(t,2);(p1(t,2)-p1(t-1,2))/deltat]);
            [sx2,modelx2] = AFKF(modelx2,[p2(t,1);(p2(t,1)-p2(t-1,1))/deltat]);
            [sy2,modely2] = AFKF(modely2,[p2(t,2);(p2(t,2)-p2(t-1,2))/deltat]);
%             [sx1,modelx1] = AFKF(modelx1,p1(t,1));
%             [sy1,modely1] = AFKF(modely1,p1(t,2));
%             [sx2,modelx2] = AFKF(modelx2,p2(t,1));
%             [sy2,modely2] = AFKF(modely2,p2(t,2));
            p_tilde1(t,:) = [sx1(1),sy1(1)];
            p_tilde2(t,:) = [sx2(1),sy2(1)];
            if dInd == 1 && pInd == 1
                speed_pred(t-2) = sqrt(sx1(2).^2+sy1(2).^2);
            end
        end

        avgErr1 = 0;
        avgErr2 = 0;
        errVec1 = zeros(n,1);
        for i = 1:n
            errVec1(i) = norm(p1(i,:)-p_tilde1(i,:));
            avgErr1 = avgErr1 + norm(p1(i,:)-p_tilde1(i,:))/n;
            avgErr2 = avgErr2 + norm(p2(i,:)-p_tilde2(i,:))/n;
        end
        errors{dInd}(pInd,:) = [avgErr1,avgErr2];
        % For plotting
        if dInd == 1 && pInd == 1
            pData.err = errVec1;
            pData.p = p1;
            speed = zeros(size(p1,1)-1,1);
            for m = 1:numel(speed)
                speed(m) = norm(p1(m+1,:) - p1(m,:));
            end
            pData.speed = speed;
        end
    end
    avgDataSetError(dInd) = mean(errors{dInd}(:));
end

%%
matrix2latex(avgDataSetError'/pxPerOneDeg, 'out.tex', 'columnLabels', dataSetNames, 'alignment', 'c', 'format', '%-6.2f');

figure;
subplot(211);
plot(pData.err,'r-*'); hold on;
xlabel('Frame #','fontsize',fontsize);
ylabel('Euclidean Error (in px)','fontsize',fontsize);
title('Error in Finite Difference Prediction','fontsize',fontsize);

subplot(212);
plot(2:size(pData.speed,1)+1,pData.speed,'k*-'); hold on;
plot(3:size(speed_pred,1)+2,speed_pred,'ro-'); hold on;
xlim([1,size(pData.err,1)])
xlabel('Frame #','fontsize',fontsize);
ylabel('Speed (in px)','fontsize',fontsize);
title('Speed Plot','fontsize',fontsize);

figure; subplot(211); plot(p1(:,1),p1(:,2),'ro-',p_tilde1(:,1),p_tilde1(:,2),'b*-');xlim(xlimits);ylim(ylimits)
legend('orig train','train estimate'); title(['Late Fusion (calculate x and y together): avg err = ',num2str(avgErr1)])
subplot(212); plot(p2(:,1),p2(:,2),'ro-',p_tilde2(:,1),p_tilde2(:,2),'b*-');xlim(xlimits);ylim(ylimits)
legend('orig testing','test estimate');
title(['Late Fusion (calculate x and y together): avg err = ',num2str(avgErr2)])
return

% for i = 1:n
% subplot(211); plot(p1(1:i,1),p1(1:i,2),'ro-',p_tilde1(1:i,1),p_tilde1(1:i,2),'b*-');xlim(xlimits);ylim(ylimits)
% legend('orig train','train estimate'); title(['Late Fusion (calculate x and y together): avg err = ',num2str(avgErr1)])
% subplot(212); plot(p2(1:i,1),p2(1:i,2),'ro-',p_tilde2(1:i,1),p_tilde2(1:i,2),'b*-');xlim(xlimits);ylim(ylimits)
% legend('orig testing','test estimate');
% title(['Late Fusion (calculate x and y together): avg err = ',num2str(avgErr2)])
% pause
% end