%% Load data

clear all;
clc;

load('../eyeData.mat');

% Video number
dataSetNum=9;
numFrames=size(data{dataSetNum}{1}{1},1);
numTotalPeople=15;
numTrialsPerPerson=2;
numEyeLoc=1;

%Number of times you tried to generate a gausssian
numFailures=0;
gaussianGenerated=false;

for frame=1:numFrames
    eyeGazeLocations=zeros(30,2);
    for numPerson=1:numTotalPeople
        for numTrial=1:numTrialsPerPerson
            eyeGazeLocations(numEyeLoc,:) =  data{dataSetNum}{numPerson}{numTrial}(frame,:); %X-Y Location for frame
            numEyeLoc=numEyeLoc+1;
        end
    end
    numEyeLoc=1;
    success=false;
    gaussianGenerated=false;
    
    while ~success
        try
            %K-means to initialize centroid locations
            [~,kmeansCenters] = kmeans(eyeGazeLocations,2);
            
            %Fit gaussian to eyeGazeLocations
            gmInitialVariance = eye(2);
            gmInitialSigma = cat(3,gmInitialVariance,gmInitialVariance);
            % Initial weights are set at 50%
            gmInitialWeights = [0.5 0.5];
            % Initial condition structure for the gmdistribution.fit function
            S.mu = kmeansCenters;
            S.Sigma = gmInitialSigma;
            S.PComponents = gmInitialWeights;
            options = statset('Display','final');
            gm = fitgmdist(eyeGazeLocations,2,'Options',options,'Start',S)
            success=true;
            gaussianGenerated=true;
        catch
            success=false;
            fprintf('Failed!');
            numFailures=numFailures+1;
            gaussianGenerated=false;
            if numFailures>5
                success=true;
            end
        end
    end
    
    if  ~gaussianGenerated
        gm = fitgmdist(eyeGazeLocations,1,'Options',options);
    end
    %scatter(eyeGazeLocations(:,1),eyeGazeLocations(:,2),10,'ko')
    %hold on
    %ezcontour(@(x,y)pdf(gm,[x y]),[0 348],[0 284]);
    %title('Scatter Plot and Fitted GMM Contour')
    %daspect([1 1 1])
    %hold off
    
    
    x = 0:1:xlimits(2); %// x axis
    y = 0:1:ylimits(2); %// y axis
    
    [X , Y] = meshgrid(x,y); %// all combinations of x, y
    if  ~gaussianGenerated
        gm = fitgmdist(eyeGazeLocations,1,'Options',options);
        Z = mvnpdf([X(:) Y(:)],gm.mu(1,:),gm.Sigma(:,:,1)); %// compute Gaussian pdf
        Z = reshape(Z,length(y),length(x));
    
    else
    Z1 = mvnpdf([X(:) Y(:)],gm.mu(1,:),gm.Sigma(:,:,1)); %// compute Gaussian pdf
    Z1 = reshape(Z1,length(y),length(x));
    
    Z2 = mvnpdf([X(:) Y(:)],gm.mu(2,:),gm.Sigma(:,:,2)); %// compute Gaussian pdf
    Z2 = reshape(Z2,length(y),length(x));
    
    Z=Z1+Z2;
    end
    subplot(2,1,1)
    plot(eyeGazeLocations(:,1),eyeGazeLocations(:,2),'*')
    subplot(2,1,2)
    imagesc(flipud(Z))
    axis([0 xlimits(2) 0 ylimits(2)]);
    
    gm.Sigma
    pause
    
end

