clc
clear
load('eyeDataBaseline.mat');
load('gaussBus.mat')
for dataset=1:1
    %dataset=1;
    tile_size=1;
    xy_seq_train =round(data{dataset}.train);
    xy_seq_test = round(data{dataset}.test);
    %discretize the states % finding velocity
    % for idx=1:numPeople
    %     for j=1:2
    %         temp= data{dataset}{idx}{j};
    %         filt_signal = medianFilt(temp,5);
    %         for row=1:size(temp,1)
    %             X_LOC(row,2*(idx-1)+j)=floor(temp(row,1)/tile_size)*tile_size+floor(tile_size/2);
    %             Y_LOC(row,2*(idx-1)+j)=floor(temp(row,2)/tile_size)*tile_size+floor(tile_size/2);
    %             x_val(row,2*(idx-1)+j)=round(temp(row,1));
    %             y_val(row,2*(idx-1)+j)=round(temp(row,2));
    %
    %             %Vel_X(row,2*(idx-1)+j)=filt_signal(row+1,1)-filt_signal(row,1);
    %             %Vel_Y(row,2*(idx-1)+j)=filt_signal(row+1,2)-filt_signal(row,2);
    %         end
    %     end
    % end
    
    PSEUDOTR_X=ones(xlimits(2));%ones(max(xy_seq_train(:,1)));
    PSEUDOTR_Y=ones(ylimits(2));%ones(max(xy_seq_train(:,2)));
    
    [tr_x,~]=hmmestimate(xy_seq_train(:,1),xy_seq_train(:,1),'Pseudotransitions',PSEUDOTR_X);
    [tr_y,~]=hmmestimate(xy_seq_train(:,2),xy_seq_train(:,2),'Pseudotransitions',PSEUDOTR_Y);
    
    sigma = 0.45*16;
   for sig_id=1:length(sigma)
    em_x = zeros(xlimits(2));%zeros(max(xy_seq_train(:,1)));
    for idx=1:size(em_x,1)  % gaussian distribution for emission in x
        em_x(idx,:)=normpdf([1:size(em_x,2)],idx+8,sigma(sig_id));
        em_x(idx,:)=em_x(idx,:)/sum(em_x(idx,:));
    end
    em_y = zeros(ylimits(2));%zeros(max(xy_seq_train(:,2)));
    
    for idx=1:size(em_y,1)  % gaussian distribution for emission in y
        em_y(idx,:)=normpdf([1:size(em_y,2)],idx+8,sigma(sig_id));
        em_y(idx,:)=em_y(idx,:)/sum(em_y(idx,:));
    end
    
    figure, imagesc(em_x);colormap(gray)
    %% estimate posterior probability for training given seq
    next_state_train=zeros(size(xy_seq_train,1),2);    
    for idx=1:size(xy_seq_train,1) % find x_state
        if (mod(idx,size(gaussBus,2))==0)
            val = size(gaussBus,2);
        else
            val = mod(idx,size(gaussBus,2));
        end
        frame_prob = sum(gaussBus{val})';
        pstate = hmmdecode(xy_seq_train(idx,1)',tr_x,em_x);
        nxt_st = tr_x*(pstate(:,end).*frame_prob);
        [val,ind]=max(nxt_st);
        next_state_train(idx,1) = ind;
    end
    
    for idx=1:size(xy_seq_train,1) % find y_state
        if (mod(idx,size(gaussBus,2))==0)
            val = size(gaussBus,2);
        else
            val = mod(idx,size(gaussBus,2));
        end
        frame_prob = sum(gaussBus{val}')';
        pstate = hmmdecode(xy_seq_train(idx,2)',tr_y,em_y);
        nxt_st = tr_y*(pstate(:,end).*frame_prob);
        [val,ind]=max(nxt_st);
        next_state_train(idx,2) = ind;
    end
    figure,plot(xy_seq_train(:,1),xy_seq_train(:,2),'bo')
    hold on, plot(next_state_train(:,1),next_state_train(:,2),'r.')
    
    %% estimate posterior probabolity for testing seq
    next_state_test=zeros(size(xy_seq_test,1),2);
    for idx=1:size(xy_seq_test,1)
        if (mod(idx,size(gaussBus,2))==0)
            val = size(gaussBus,2);
        else
            val = mod(idx,size(gaussBus,2));
        end
        frame_prob = sum(gaussBus{val})';
        pstate = hmmdecode(xy_seq_test(idx,1)',tr_x,em_x);
        nxt_st = tr_x*(pstate(:,end).*frame_prob);
        [val,ind]=max(nxt_st);
        next_state_test(idx,1) = ind;
    end
    for idx=1:size(xy_seq_test,1)
        if (mod(idx,size(gaussBus,2))==0)
            val = size(gaussBus,2);
        else
            val = mod(idx,size(gaussBus,2));
        end
        frame_prob = sum(gaussBus{val}')';
        pstate = hmmdecode(xy_seq_test(idx,2)',tr_y,em_y);
        nxt_st = tr_y*pstate(:,end);
        [val,ind]=max(nxt_st);
        next_state_test(idx,2) = ind;
    end
    
    figure,plot(xy_seq_test(:,1),xy_seq_test(:,2),'bo')
    hold on, plot(next_state_test(:,1),next_state_test(:,2),'r.')
    
    %% evaluate MSE for training and testing data
    
    TRAINING_MSE(sig_id) = sqrt(sum(sum((xy_seq_train-next_state_train).^2'))/size(xy_seq_train,1));
    TESTING_MSE(sig_id) = sqrt(sum(sum((xy_seq_test-next_state_test).^2'))/size(xy_seq_test,1));
    end
end
TRAINING_MSE
TESTING_MSE


% %% estimate posterior probability for testing seq
next_state_test_300=zeros(size(xy_seq_test,1),1);
time=300
for idx=1:time
    pstate = hmmdecode(xy_seq_test(idx,1)',tr_x,em_x);  
    nxt_st = tr_x*pstate(:,end);
    [val,ind]=max(nxt_st);
    next_state_test_300(idx) = ind;
end
next_st = pstate;
for idx=time+1:size(xy_seq_test,1)
    if (mod(idx,size(gaussBus,2))==0)
        val = size(gaussBus,2);
    else
        val = mod(idx,size(gaussBus,2));
    end
    frame_prob = sum(gaussBus{val})';
    next_st = tr_x* next_st;
    [val,ind]=max(next_st);
    next_state_test_300(idx) = ind;
end

figure,plot(xy_seq_test(:,1))
hold on, plot(next_state_test_300,'r--')
% 
% %%
% for idx=23:35
%     %plot(xy_seq_test(idx,1),xy_seq_test(idx,2),'b.')
%     quiver( xy_seq_test(idx,1),xy_seq_test(idx,2),xy_seq_test(idx+1,1)-xy_seq_test(idx,1),xy_seq_test(idx+1,2)-xy_seq_test(idx,2),0,'b','maxheadsize',0.1,'Linewidth',2);
%     hold on
%     quiver( next_state_test(idx,1),next_state_test(idx,2),next_state_test(idx+1,1)-next_state_test(idx,1),next_state_test(idx+1,2)-next_state_test(idx,2),0,'r','maxheadsize',0.2,'Linewidth',2);
%     quiver( xy_seq_test(idx,1),xy_seq_test(idx,2),next_state_test(idx,1)-xy_seq_test(idx,1),next_state_test(idx,2)-xy_seq_test(idx,2),0,'g--','maxheadsize',0.2,'Linewidth',2);
%     
% %     plot(next_state_test(idx,1),next_state_test(idx,2),'r*')
% %     axis([min(xy_seq_test(:,1)) max(xy_seq_test(:,1)) min(xy_seq_test(:,1)) max(xy_seq_test(:,1)) ])
% %     title (num2str(idx))
% %     pause(0.1)
% end
% 
% %drawArrow = @(x,y) quiver( x(1),y(1),x(2)-x(1),y(2)-y(1),0,'maxheadsize',1);
