clear; clc;
load('../eyeData.mat');


% Video number
dataSetNum=1;

numTotalPeople=15;
numTrialsPerPerson=2;

% Number of people to use for training
numPeopleTrain=15;
%70 percent of trials used for training
maxTrainingTrials=21;

% Velocities are in pixels per second
fixationThreshVelocity = 5;
pursuitThreshVelocity  = 50;
frameRate=30;


for numPerson=1:numTotalPeople
    for numTrial=1:numTrialsPerPerson
        %(X , Y) - locations
        eyePositions{numPerson}{numTrial} = data{dataSetNum}{numPerson}{numTrial};
        filteredPositions=eyePositions{numPerson}{numTrial};
        numFrames=size(eyePositions{numPerson}{numTrial},1);
        
        % Plot filtered and original points
        %plot(eyePositions(:,1),eyePositions(:,2),'ro')
        %hold on
        %plot(filteredPositions(:,1),filteredPositions(:,2),'g*')
        
        eyeVelocity=zeros(numFrames,2);
        eyeVelocity(1:numFrames-1,:) = diff(filteredPositions);
        eyeVelocity(numFrames)=eyeVelocity(numFrames-1);
        
        for frame=1:numFrames
            normVelocity(frame)=norm(eyeVelocity(frame,:));
            if(normVelocity(frame)<fixationThreshVelocity)
                eyeStates{numPerson}{numTrial}(frame)=1;
            elseif(normVelocity(frame)<pursuitThreshVelocity)
                eyeStates{numPerson}{numTrial}(frame)=2;
            else
                eyeStates{numPerson}{numTrial}(frame)=3;
            end
        end
        
        plot(normVelocity,'b');
        hold on
        stem(eyeStates{numPerson}{numTrial}*50,'g');                       
    end   
    
end

trainStates=[];
trainEmissions=[];

%Make training emissions and states
for j=1:1 %Trial
    for i=1:1   %Person  
        trainStates=[trainStates;eyeStates{i}{j}'];
        trainEmissions=[trainEmissions;eyePositions{i}{j}];
    end
end
trainEmissions=floor(trainEmissions+1)';


testStates=[];
testEmissions=[];
%Make testing emissions and states
for j=2:2 %Trial
    for i=11:11   %Person  
        testStates=[testStates;eyeStates{i}{j}'];
        testEmissions=[testEmissions;eyePositions{i}{j}];
    end
end
testEmissions=floor(testEmissions+1)';

%% Estimate HMM transition probabilities:
[TransX,EmisX] = hmmestimate(trainEmissions(1,:),trainStates,'Pseudoemissions',ones(3,352));

%% Get states from observations
predictedStateProb = hmmdecode(testEmissions(1,:),TransX,EmisX);
[~,predictedStates]=max(predictedStateProb);

viterbiStates=hmmviterbi(testEmissions(1,:),TransX,EmisX);
%% Compare
diff=abs(testStates'-predictedStates);
diff = diff~=0;
numIncorrectStates = sum(diff);
percentError = (numIncorrectStates/length(testStates))*100

figure
plot(viterbiStates);