function [filt_signal] = medianFilt(signal, windowSize)

win = windowSize;
filt_signal = zeros(size(signal,1),size(signal,2));
filt_signal(1:win,:) = signal(1:win,:);

for j=1:size(signal,2)
    for i=win:size(signal,1)-win
        filt_signal(i,j) = median(signal(i-(win-1):i,j));
    end
    filt_signal(i:end,j) = signal(i:end,j);
end

end