clear all; clc; close all;
load eyeData;
dataset = 2;
person = 1;
trial = 1;
k=2;
x = [-10:0.1:10]';
%data = {{{[x,x.^2],[x,x.^2+3*rand(size(x))]}}};
%data = {{{[x,cos(x)],[x,cos(x)+rand(size(x))]}}};
n = size(data{dataset}{person}{trial},1);
%% Late Fusion (calculate x and y separately)
Ax = zeros(n-k,k);
Ay = zeros(n-k,k);
zx = zeros(n-k,1);
zy = zeros(n-k,1);
for i = k+1:n
    %plot(data{dataset}{person}{trial}(1:i,1),...
    % data{dataset}{person}{trial}(1:i,2),'*-')
    %pause
    zx(i-k) = data{dataset}{person}{trial}(i,1);
    zy(i-k) = data{dataset}{person}{trial}(i,2);
    Ax(i-k,:) = data{dataset}{person}{trial}(i-k:i-1,1)';
    Ay(i-k,:) = data{dataset}{person}{trial}(i-k:i-1,2)';
end
% Bias term
Ax = [Ax,ones(size(Ax,1),1)];
Ay = [Ay,ones(size(Ay,1),1)];
wx = Ax\zx;
wy = Ay\zy;
bx = wx(end);
by = wy(end);
wx(end) = [];
wy(end) = [];

MSE1 = 0;
MSE2 = 0;
p1 = [];
p_tilde1 = [];
p2 = [];
p_tilde2 = [];
for i = k+1:n
    p1(:,end+1) = data{dataset}{person}{1}(i,1:2)';
    p_tilde1(:,end+1) = [data{dataset}{person}{1}(i-k:i-1,1)'*wx+bx;
               data{dataset}{person}{1}(i-k:i-1,2)'*wy+by];
    MSE1 = MSE1 + norm(p1(:,end)-p_tilde1(:,end));
    %
    p2(:,end+1) = data{dataset}{person}{2}(i,1:2)';
    p_tilde2(:,end+1) = [data{dataset}{person}{2}(i-k:i-1,1)'*wx+bx;
               data{dataset}{person}{2}(i-k:i-1,2)'*wy+by];
    MSE2 = MSE2 + norm(p2(:,end)-p_tilde2(:,end));
end
disp('Late Fusion (calculate x and y separately)')
MSE1=MSE1/n;MSE2=MSE2/n;
MSE1,MSE2
figure; subplot(211); plot(p1(1,:),p1(2,:),'ro-',p_tilde1(1,:),p_tilde1(2,:),'b*-');xlim(xlimits);ylim(ylimits)
legend('orig train','train estimate'); title(['Late Fusion (calculate x and y separately): avg err = ',num2str(MSE1)])
subplot(212); plot(p2(1,:),p2(2,:),'ro-',p_tilde2(1,:),p_tilde2(2,:),'b*-');xlim(xlimits);ylim(ylimits)
legend('orig testing','test estimate');
title(['Late Fusion (calculate x and y separately): avg err = ',num2str(MSE2)])
%% Late Fusion (calculate x and y together)
A = [];%zeros(2*(n-k),k);
z = [];%zeros(2*(n-k),1);
for i = k+1:n
    %z(i-k:i-k+1) = data{dataset}{person}{trial}(i,:)';
    %A(i-k:i-k+1,:) = data{dataset}{person}{trial}(i-k:i-1,:)';
    z = [z;data{dataset}{person}{trial}(i,:)'];
    A = [A;data{dataset}{person}{trial}(i-k:i-1,:)'];
end
w = A\z;
MSE1 = 0;
MSE2 = 0;
p1 = [];
p_tilde1 = [];
p2 = [];
p_tilde2 = [];
for i = k+1:n
    p1(:,end+1) = data{dataset}{person}{1}(i,1:2)';
    p_tilde1(:,end+1) = [data{dataset}{person}{1}(i-k:i-1,:)'*w];
    MSE1 = MSE1 + norm(p1(:,end)-p_tilde1(:,end));
    p2(:,end+1) = data{dataset}{person}{2}(i,1:2)';
    p_tilde2(:,end+1) = [data{dataset}{person}{2}(i-k:i-1,:)'*w];
    MSE2 = MSE2 + norm(p2(:,end)-p_tilde2(:,end));
    
    subplot(211); plot(p1(1,:),p1(2,:),'ro-',p_tilde1(1,:),p_tilde1(2,:),'b*-');%xlim(xlimits);ylim(ylimits)
    legend('orig train','train estimate'); title(['Late Fusion (calculate x and y together): avg err = ',num2str(MSE1)])
    subplot(212); plot(p2(1,:),p2(2,:),'ro-',p_tilde2(1,:),p_tilde2(2,:),'b*-');%xlim(xlimits);ylim(ylimits)
    legend('orig testing','test estimate');
    title(['Late Fusion (calculate x and y together): avg err = ',num2str(MSE2)])
    pause
end
disp('---------------------------------------')
disp('Late Fusion (calculate x and y together)')
MSE1=MSE1/n;MSE2=MSE2/n;
MSE1,MSE2
figure; subplot(211); plot(p1(1,:),p1(2,:),'ro-',p_tilde1(1,:),p_tilde1(2,:),'b*-');xlim(xlimits);ylim(ylimits)
legend('orig train','train estimate'); title(['Late Fusion (calculate x and y together): avg err = ',num2str(MSE1)])
subplot(212); plot(p2(1,:),p2(2,:),'ro-',p_tilde2(1,:),p_tilde2(2,:),'b*-');xlim(xlimits);ylim(ylimits)
legend('orig testing','test estimate');
title(['Late Fusion (calculate x and y together): avg err = ',num2str(MSE2)])
%% Matrix Fusion (calculate x and y together)
A = [];%zeros(2*(n-k),k);
z = [];%zeros(2*(n-k),1);
for i = k+1:n
    %z(i-k:i-k+1) = data{dataset}{person}{trial}(i,:)';
    %A(i-k:i-k+1,:) = data{dataset}{person}{trial}(i-k:i-1,:)';
    z = [z;data{dataset}{person}{trial}(i,:)'];
    blkmat = [];
    for j = k:-1:1
        blk = zeros(2,4);
        blk(1,1) = data{dataset}{person}{trial}(i-j,1);
        blk(2,2) = data{dataset}{person}{trial}(i-j,1);
        blk(1,3) = data{dataset}{person}{trial}(i-j,2);
        blk(2,4) = data{dataset}{person}{trial}(i-j,2);
        %blkmat = blkdiag(blkmat,blk);
        blkmat = [blkmat,blk];
    end
    A = [A;blkmat];
end
w = A\z;
MSE1 = 0;
MSE2 = 0;
p1 = [];
p_tilde1 = [];
p2 = [];
p_tilde2 = [];
for i = k+1:n
    p1(:,end+1) = data{dataset}{person}{1}(i,1:2)';
    p_tilde1(:,end+1) = zeros(2,1);
    ind = 1;
    for j = k:-1:1
        w_cur = reshape(w(ind:ind + 4-1),2,2); ind = ind + 4;
        p_tilde1(:,end) = p_tilde1(:,end) + w_cur*data{dataset}{person}{1}(i-j,:)';
    end
    MSE1 = MSE1 + norm(p1(:,end)-p_tilde1(:,end));
    %
    p2(:,end+1) = data{dataset}{person}{2}(i,1:2)';
    p_tilde2(:,end+1) = zeros(2,1);
    ind = 1;
    for j = k:-1:1
        w_cur = reshape(w(ind:ind + 4-1),2,2); ind = ind + 4;
        p_tilde2(:,end) = p_tilde2(:,end) + w_cur*data{dataset}{person}{2}(i-j,:)';
    end
    MSE2 = MSE2 + norm(p2(:,end)-p_tilde2(:,end));
end
disp('---------------------------------------')
disp('Matrix Fusion (calculate x and y together)')
MSE1=MSE1/n;MSE2=MSE2/n;
MSE1,MSE2
figure; subplot(211); plot(p1(1,:),p1(2,:),'ro-',p_tilde1(1,:),p_tilde1(2,:),'b*-');xlim(xlimits);ylim(ylimits)
legend('orig train','train estimate'); title(['Matrix Fusion (calculate x and y together): avg err = ',num2str(MSE1)])
subplot(212); plot(p2(1,:),p2(2,:),'ro-',p_tilde2(1,:),p_tilde2(2,:),'b*-');xlim(xlimits);ylim(ylimits)
legend('orig testing','test estimate');
title(['Matrix Fusion (calculate x and y together): avg err = ',num2str(MSE2)])