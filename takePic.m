function image=takePic(camera,rect)

image=snapshot(camera);
image = imcrop(flipud(image),rect);
% h = figure;
% imagesc(image)
% pause
% close(h) 
end