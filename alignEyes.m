function [eyeRect, referenceImage] = alignEyes(camera)

preview(camera)

if exist('masterRefImage.mat', 'file')
    fprintf('Master reference exists. Continue...\n');
    load('masterRefImage.mat');
else
    fprintf('Master reference not found, please take reference image\n')
    masterRefImage=takeRefImage(camera,'master');
    save masterRefImage.mat masterRefImage
end

% Take reference image for current data set
fprintf('Take reference image for current data set\n')
alignmentComplete=false;

while(~alignmentComplete)
    close all
    trainRefImage =takeRefImage(camera,'train');
    referenceImage=trainRefImage;
    load('masterRefImage.mat');
    imagesc(referenceImage-masterRefImage);
    %[alignmentComplete, eyeRect]=getAlignmentRect(trainRefImage,masterRefImage);
    eyeRect=[];
    choice = questdlg('Is the image OK?','Image check','Yes','No','Yes');
    if strcmp(choice,'Yes')
        alignmentComplete = true;
    end
end
closePreview(camera)
end

function [croppedImage]=takeRefImage(camera,mode)

imageOK=false;
while(~imageOK)
    
    pause
    image=snapshot(camera);
    image=flipud(image);
    imhandle=figure;
    imshow(image)
    
    choice = questdlg('Is the image OK?','Image check','Yes','No','Yes');
    if strcmp(choice,'Yes')
        imageOK = true;
    else
        close(imhandle);
    end
end

if strcmp(mode,'master')
    fprintf('Select cropping points\n')
    %[x,y] = ginput(2);
    %rect=[x(1) y(1) x(2)-x(1) y(2)-y(1)];
    %rectangle('position',rect,'EdgeColor','b')
    croppedImage = image;%= imcrop(image,rect);
    figure
    imagesc(croppedImage);
elseif strcmp(mode,'train')
    croppedImage = image;
    figure
    imagesc(croppedImage);
end

end