function [aligned,rect] = getAlignmentRect(trainingRef,masterRef)
%

f2=trainingRef;
f1=masterRef;

c = normxcorr2(rgb2gray(f1),rgb2gray(f2));
% Offset found by correlation
[ypeak, xpeak] = find(c==max(c(:)));
yoffSet = ypeak-size(f1,1);
xoffSet = xpeak-size(f1,2);
%
figure
sizeRect=size(masterRef);
rect=[xoffSet yoffSet sizeRect(2) sizeRect(1)];
imagesc(trainingRef);
hold on
rectangle('position',rect,'EdgeColor','b');

if( xoffSet < 0 || xoffSet+sizeRect(2) > size(f2,2) || yoffSet < 0 || yoffSet+sizeRect(1) > size(f2,1) )
    aligned=false;
else
    aligned=true;
end
pause

