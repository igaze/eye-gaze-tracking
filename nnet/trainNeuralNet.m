function [testingLabel,trainingLabel,avgTestDistError,avgTrainDistError] = trainNeuralNet(trainingData,trainingLabelTrue,testingData,testingLabelTrue,trainFcn,hiddenLayerSize,trainPercent)
if nargin < 5
    trainFcn = 'trainbr';
end
if nargin < 6
    hiddenLayerSize=20;
end
if nargin < 7
    trainPercent=85/100;
end
    
%TRAINNEURALNET Trains the neural net and outputs training and testing errors

% Solve an Input-Output Fitting problem with a Neural Network
x = trainingData';
t = trainingLabelTrue';

% Choose a Training Function
% For a list of all training functions type: help nntrain
% 'trainlm' is usually fastest.
% 'trainbr' takes longer but may be better for challenging problems.
% 'trainscg' uses less memory. Suitable in low memory situations.
%trainFcn = 'trainbr';  % Bayesian Regularization backpropagation.

% Create a Fitting Network
% hiddenLayerSize = 10;
net = fitnet(hiddenLayerSize,trainFcn);

% Choose Input and Output Pre/Post-Processing Functions
% For a list of all processing functions type: help nnprocess
net.input.processFcns = {'removeconstantrows','mapminmax'};
net.output.processFcns = {'removeconstantrows','mapminmax'};

% Setup Division of Data for Training, Validation, Testing
% For a list of all data division functions type: help nndivide
net.divideFcn = 'dividerand';  % Divide data randomly
net.divideMode = 'sample';  % Divide up every sample
net.divideParam.trainRatio = trainPercent;
net.divideParam.valRatio = 1-trainPercent;
net.divideParam.testRatio = 0/100;

% Choose a Performance Function
% For a list of all performance functions type: help nnperformance
net.performFcn = 'mse';  % Mean Squared Error


% Train the Network
[net,tr] = train(net,x,t);

% Test the Network
y = net(x);
e = gsubtract(t,y);
performance = perform(net,t,y)

% Recalculate Training, Validation and Test Performance
trainTargets = t .* tr.trainMask{1};
valTargets = t .* tr.valMask{1};
testTargets = t .* tr.testMask{1};
trainPerformance = perform(net,trainTargets,y)
valPerformance = perform(net,valTargets,y)
%testPerformance = perform(net,testTargets,y)

%%
%Get output on test data
testingLabel = net(testingData')';
trainingLabel = net(trainingData')';

% error
avgTestDistError = 0;
avgTrainDistError = 0;
for i = 1:size(testingLabel,1)
    avgTestDistError = avgTestDistError + norm(testingLabel(i,:)-testingLabelTrue(i,:))/size(testingLabel,1);
end
for i = 1:size(trainingLabel,1)
    avgTrainDistError = avgTrainDistError + norm(trainingLabel(i,:)-trainingLabelTrue(i,:))/size(trainingLabel,1);
end

%end

