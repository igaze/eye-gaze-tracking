function plotErrors(testingLabel,testingLabelTrue,trainingLabel,trainingLabelTrue,testingError,trainingError)
% 
data = load('../data/data_all.mat');
%
figure;
subplot(211);
markersize=3;
fontsize=15;
plot(testingLabel(:,1),testingLabel(:,2),'r*',testingLabelTrue(:,1),testingLabelTrue(:,2),'bo','markersize',markersize); legend({'est','true'},'location','northeastoutside')
title(sprintf('Test: avg L2 error = %0.2f',testingError),'fontsize',fontsize)

line([testingLabel(:,1)';testingLabelTrue(:,1)'],...
    [testingLabel(:,2)';testingLabelTrue(:,2)'],'color','g')
daspect([1,1,1])
xlim([data.xmin-5,data.xmax+5])
ylim([data.ymin-5,data.ymax+5])

subplot(212);
plot(trainingLabel(:,1),trainingLabel(:,2),'r*',trainingLabelTrue(:,1),trainingLabelTrue(:,2),'bo','markersize',markersize); legend({'est','true'},'location','northeastoutside')
title(sprintf('Train: || avg L2 error = %0.2f',trainingError),'fontsize',fontsize)

line([trainingLabel(:,1)';trainingLabelTrue(:,1)'],...
    [trainingLabel(:,2)';trainingLabelTrue(:,2)'],'color','g')
daspect([1,1,1])
xlim([data.xmin-5,data.xmax+5])
ylim([data.ymin-5,data.ymax+5])
end