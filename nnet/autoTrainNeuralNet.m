%Automate neural net training
clear all; clc;
trainingPercentage = 0.7;

cd ../data/
[Train,Train_label,Test,Test_label]=selectCrossValRand(trainingPercentage);
cd ../nnet/

%% TRAINLM
% algorithm = 'trainlm'; 
% iter=1;
% for hiddenLayerSize=5:5:20
%     
% [testingLabel_lm{iter},trainingLabel_lm{iter},avgTestDistError_lm{iter},avgTrainDistError_lm{iter}] = trainNeuralNet(Train,Train_label,Test,Test_label,algorithm,hiddenLayerSize);
% iter=iter+1;
% 
% end
% 
% save('NeuralNetData_lm.mat');
% 

%% TRAINBR
algorithm = 'trainbr'; 
iter=1;
for hiddenLayerSize=5:5:20
    
[testingLabel_br{iter},trainingLabel_br{iter},avgTestDistError_br{iter},avgTrainDistError_br{iter}] = trainNeuralNet(Train,Train_label,Test,Test_label,algorithm,hiddenLayerSize);
iter=iter+1;

end

save('NeuralNetData_br.mat');

%% TRAINSCG
% algorithm = 'trainscg'; 
% iter=1;
% for hiddenLayerSize=5:5:20
%     
% [testingLabel_scg{iter},trainingLabel_scg{iter},avgTestDistError_scg{iter},avgTrainDistError_scg{iter}] = trainNeuralNet(Train,Train_label,Test,Test_label,algorithm,hiddenLayerSize);
% iter=iter+1;
% 
% end
% 
% save('NeuralNetData_scg.mat');