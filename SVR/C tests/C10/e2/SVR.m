function [testingLabel,trainingLabel,avgTestDistError,avgTrainDistError] = SVR(trainingData,trainingLabelTrue,testingData,testingLabelTrue,epsilon,C,varargin)
% - Rows are training examples and columns are features
% - Labels should be integers
if nargin < 5
    epsilon = 1;
end
if nargin < 6
    C = 50;
end
if nargin < 7
    kernelName = 'linear';
else
    kernelName = varargin{1};
end

if strcmp(kernelName,'gaussian')
    lambda = varargin{2};
    kernel_function = @(x1,x2) exp(-lambda*norm(x1-x2,2)^2);
elseif strcmp(kernelName,'spline')
    kernel_function = @(a,b) prod(arrayfun(@(x,y) 1 + x*y+x*y*min(x,y)-(x+y)/2*min(x,y)^2+1/3*min(x,y)^3,a,b));
elseif strcmp(kernelName,'periodic')
    l = varargin{2};
    p = varargin{3};
    kernel_function = @(x1,x2) exp(-2*sin(pi*norm(x1-x2,2)/p)^2/l^2); 
elseif strcmp(kernelName,'tangent')
    a = varargin{2};
    c = varargin{3};
    kernel_function = @(x1,x2) prod(tanh(a*x1'*x2+c)); 
elseif strcmp(kernelName,'polynomial')
    % K(x,y) = (a*x'*y + b)^n
    a = varargin{2};
    b = varargin{3};
    n = varargin{4};
    kernel_function = @(x1,x2) (a*x1'*x2+b)^n;
elseif strcmp(kernelName,'linear')
    kernel_function = @(x1,x2) x1'*x2;
end

% Train Model
% tic
% [W,b] = cvx_SVR(trainingData,trainingLabelTrue,epsilon,C);
% fprintf('primal: %fs\n',toc)
% tic
[W,b] = cvx_SVR_dual(trainingData,trainingLabelTrue,epsilon,C,kernel_function);
% fprintf('dual: %fs\n',toc)

%% Primal Error
testingLabel = zeros(size(testingData,1),2);
%testingLabel(:,1) = testingData*W(:,1) + b(1);
%testingLabel(:,2) = testingData*W(:,2) + b(2);
testingLabel(:,1) = eval_kernel(testingData,W(:,1),kernel_function) + b(1);
testingLabel(:,2) = eval_kernel(testingData,W(:,2),kernel_function) + b(2);

trainingLabel = zeros(size(trainingData,1),2);
%trainingLabel(:,1) = trainingData*W(:,1) + b(1);
%trainingLabel(:,2) = trainingData*W(:,2) + b(2);
trainingLabel(:,1) = eval_kernel(trainingData,W(:,1),kernel_function) + b(1);
trainingLabel(:,2) = eval_kernel(trainingData,W(:,2),kernel_function) + b(2);

% error
avgTestDistError = 0;
avgTrainDistError = 0;
for i = 1:size(testingLabel,1)
    avgTestDistError = avgTestDistError + norm(testingLabel(i,:)-testingLabelTrue(i,:))/size(testingLabel,1);
end
for i = 1:size(trainingLabel,1)
    avgTrainDistError = avgTrainDistError + norm(trainingLabel(i,:)-trainingLabelTrue(i,:))/size(trainingLabel,1);
end

%% Dual Error
% testingLabel2 = zeros(size(testingData,1),2);
% testingLabel2(:,1) = testingData*W2(:,1) + b2(1);
% testingLabel2(:,2) = testingData*W2(:,2) + b2(2);
% 
% trainingLabel2 = zeros(size(trainingData,1),2);
% trainingLabel2(:,1) = trainingData*W2(:,1) + b2(1);
% trainingLabel2(:,2) = trainingData*W2(:,2) + b2(2);
% 
% % error
% avgTestDistError2 = 0;
% avgTrainDistError2 = 0;
% for i = 1:size(testingLabel2,1)
%     avgTestDistError2 = avgTestDistError2 + norm(testingLabel2(i,:)-testingLabelTrue(i,:))/size(testingLabel2,1);
% end
% for i = 1:size(trainingLabel2,1)
%     avgTrainDistError2 = avgTrainDistError2 + norm(trainingLabel2(i,:)-trainingLabelTrue(i,:))/size(trainingLabel2,1);
% end
% 
% fprintf('testing error: primal = [%f], dual = [%f]\n',avgTestDistError,avgTestDistError2)
% fprintf('training error: primal = [%f], dual = [%f]\n',avgTrainDistError,avgTrainDistError2)
% 
% W = W2;
% b = b2;
end

function result = eval_kernel(X,w,kernel)
result = zeros(size(X,1),1);
for i = 1:length(result)
    result(i) = kernel(X(i,:)',w);
end
end

function [W,b] = cvx_SVR(X,y,epsilon,C)
% min_{w,b} 1/2 ||w||^2 + C(sum(slack1 + slack2))
% s.t       y^(i) - w^T x^(i) - b <= epsilon + slack1(i)
%           w^T x^(i) + b - y^(i) <= epsilon + slack2(i)
    n = size(X,2);
    m = size(X,1);
    W = zeros(n,2); %[wx,wy]
    b = [0,0];      %[bx,by]
    for dim = [1,2]
        cvx_begin quiet
            variables w(n) b_cvx eta1(m) eta2(m)
            if ~isinf(C)
                minimize(1/2*w'*w+C*sum(eta1+eta2))
            else
                minimize(sum(eta1+eta2))
            end
            subject to
                for i = 1:m
                    y(i,dim) - w'*X(i,:)' - b_cvx <= epsilon + eta1(i)
                    w'*X(i,:)' + b_cvx - y(i,dim) <= epsilon + eta2(i)
                    eta1(i) >= 0
                    eta2(i) >= 0
                end
        cvx_end
        W(:,dim) = w;
        b(dim) = b_cvx;
    end
end

%% Note: choice of b is very bad when n small
function [W,b] = cvx_SVR_dual(X,y,epsilon,C,kernel)
% http://alex.smola.org/papers/2003/SmoSch03b.pdf
% PRIMAL
%
%       min_{w,b} 1/2 ||w||^2 + Csum_i^l[eta1(i)+eta2(i)]
%       s.t       y^(i) - <w,x^(i)> - b <= epsilon + eta1(i)
%                 <w,x^(i)> + b - y^(i) <= epsilon + eta2(i)
%
% DUAL
%
%       max_{alpha1,alpha2} -1/2 * sum_i^l,sum_j^l[(alpha1(i)-alpha2(i))(alpha1(j)-alpha2(j))<x(i),x(j)>]
%                           -epsilon * sum_i^l[alpha1(i)-alpha2(i)] + sum_i^l[y(i)*(alpha1(i)-alpha2(i))]
%       s.t       sum_i^l[alpha1(i) - alpha2(i)] = 0
%                 0 <= alpha1(i),alpha2(i) <= C
%           
% We can then get the primal solution as follows:
%   w = sum_i^l[(alpha1(i)-alpha2(i))*x(i)]
%   f(x) = sum_i^l[(alpha1(i)-alpha2(i))*<x(i),x>] + b
    if nargin < 4
        C = inf;
    end
    
    n = size(X,2);
    m = size(X,1);
    W = zeros(n,2); %[wx,wy]
    b = [0,0];      %[bx,by]

    % Process Linear Kernel
    K = zeros(m,m);
    for i = 1:size(K,1)
        for j = 1:size(K,2)
            %K(i,j) = (X(i,:)*X(j,:)');
            K(i,j) = kernel(X(i,:)',X(j,:)');
        end
    end
    
    for dim = [1,2]
        cvx_begin quiet
            variables alpha1(m) alpha2(m) 
            OBJ = - 1/2*quad_form(alpha1-alpha2,K);
            OBJ = OBJ - epsilon*sum(alpha1+alpha2) + sum(y(:,dim).*(alpha1-alpha2));
            maximize(OBJ)
            subject to
                sum(alpha1 - alpha2) == 0
                alpha1 >= 0
                alpha2 >= 0
                if ~isinf(C)
                    alpha1 <= C
                    alpha2 <= C
                end
        cvx_end
        
        bmax = -inf;
        bmin = inf;
        for i = 1:m
            W(:,dim) = W(:,dim) + (alpha1(i) - alpha2(i))*X(i,:)';
        end
        for j = 1:m
            if alpha1(j) < C || alpha2(j) > 0
                bmax = max(bmax,-epsilon+y(j,dim)-kernel(W(:,dim),X(j,:)'));
            end
            if alpha1(j) > 0 || alpha2(j) < C
                bmin = min(bmin,-epsilon+y(j,dim)-kernel(W(:,dim),X(j,:)'));
            end
        end
        b(dim) = (bmax+bmin)/2;
    end
end