clear all; clc; close all; 
% Parameters
chooseBestModelParameterManually = false;
parameterIndex = 6; %6 for 160 feat, 3 for 130feat no eigen %8 for 350 feat 0.4
trialsnum = 1;
trainingPercentage = 0.7;
testingPercentage = 1-trainingPercentage;
fontsize = 15;
markersize = 3;

%% DATA
data = load('../../../../data/data_all.mat');

%% Least squares (with and without) regularization
C = [200]; ei = 4;
epsilon = linspace(1e-10,10,20);
epsilon = epsilon( (ei-1)*5+1:5*ei );
[epsMesh,CMesh] = meshgrid(epsilon,C);
% rows are C and cols are epsilon

testingError = zeros(length(C),length(epsilon));
trainingError = zeros(length(C),length(epsilon));
trainingLabel_cell = cell(length(C),length(epsilon));
testingLabel_cell = cell(length(C),length(epsilon));
for i = 1:numel(epsMesh)
    fprintf('model i = %d/%d\n',i,numel(epsMesh))
    for tnum = 1:trialsnum
        rseed = tnum;
        cd ../../../../data
        [trainingData,trainingLabelTrue,testingData,testingLabelTrue] = selectCrossValRand(trainingPercentage,rseed);
        %trainingData(:,end-29:end) = [];
        %testingData(:,end-29:end) = [];
        cd(sprintf('../SVR/C tests/C%d/e%d',C,ei))
        if i == 1 && tnum == 1
            disp('training size')
            size(trainingData)
            disp('testing size')
            size(testingData)
        end
        [testingLabel,trainingLabel,avgTestDistError,avgTrainDistError] = SVR(trainingData,trainingLabelTrue,testingData,testingLabelTrue,epsMesh(i),CMesh(i),'polynomial',1,1/2,2);
        testingError(i) = testingError(i) + avgTestDistError;
        trainingError(i) = trainingError(i) + avgTrainDistError;
        % technically this only records the last set of data (should probably be corrected)
        testingLabel_cell{i} = testingLabel;
        trainingLabel_cell{i} = trainingLabel;
    end
    testingError(i) = testingError(i) / trialsnum;
    trainingError(i) = trainingError(i) / trialsnum;
end

clear data; % prevent saved workspace from getting too big
save('SVRModelData.mat')