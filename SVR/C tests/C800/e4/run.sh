#!/bin/bash

# Name the job in Grid Engine
#$ -N SVRTest800e4

#tell grid engine to use current directory
#$ -cwd

# Set Email Address where notifications are to be sent
#$ -M tckong@stanford.edu

# Tell Grid Engine to notify job owner if job 'b'egins, 'e'nds, 's'uspended is 'a'borted, or 'n'o mail
#$ -m besa

# Tel Grid Engine to join normal output and error output into one file 
#$ -j y


## the "meat" of the script

#print the name of this machine
hostname
#also print the date
date
echo start
module load matlab
matlab -nodisplay -nosplash -r testSVR
echo end
data
# run it with qsub run.sh on any farmshare server
