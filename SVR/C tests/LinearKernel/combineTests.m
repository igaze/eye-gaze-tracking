clear all; clc; close all;
fontsize = 15;
markersize = 4;
C = [10,40,70,200,800,10000];
colors = distinguishable_colors(length(C));
plotnames = {};
plothandles = [];
minC = -1;
minTestError = inf;
minEps = -1;
minData = [];
minInd = -1;
for i = 1:length(C)
    cd(sprintf('C%d',C(i)))
    data = load('SVRModelData.mat');
    cd ..
    [bestTestError,bestInd] = min(data.testingError);
    if bestTestError < minTestError
        minInd = bestInd;
        minTestError = bestTestError;
        minC = C(i);
        minEps = data.epsilon(bestInd);
        minData = data;
    end
    h = plot(data.epsilon,data.testingError,'o-','color',colors(i,:)); hold on;
    plot(data.epsilon(bestInd),bestTestError,'ko','markersize',markersize,'markerfacecolor','k');
    plothandles = [plothandles,h];
    plotnames{end+1} = sprintf('C = %d',data.C);
end
legend(plothandles,plotnames,'location','northwest','fontsize',fontsize)
title(sprintf('SVR model Tradeoff'),'fontsize',18)
xlabel('\epsilon','fontsize',fontsize)
ylabel('Average L2 DistError','fontsize',fontsize)
print('-depsc',sprintf('AllTestErrorCombined'))
set(gca,'fontsize',fontsize)
%%
% figure; 
% plot(minData.epsilon,minData.testingError,'bo-',...
%      minData.epsilon,minData.trainingError,'m^-'); hold on;
% plot(minEps,minTestError,'r*');
% legend('testingError','trainingError','Best Model Choice')
% title('SVR [Best] Train/Test Tradeoff','fontsize',18)
% xlabel('\epsilon','fontsize',fontsize)
% ylabel('Average L2 DistError','fontsize',fontsize)
% print -depsc BestTestANDTrainError

data = load('../../../data/data_all.mat');
figure;
subplot(211);
plot(minData.testingLabel_cell{minInd}(:,1),minData.testingLabel_cell{minInd}(:,2),'r*',minData.testingLabelTrue(:,1),minData.testingLabelTrue(:,2),'bo','markersize',markersize); legend({'est','true'},'location','northeastoutside')
title(sprintf('[BEST] Test: eps = [%0.2f], C = [%0.2f] || avg L2 error = %0.2f',minEps,minC,min(minData.testingError)),'fontsize',fontsize)
line([minData.testingLabel_cell{minInd}(:,1)';minData.testingLabelTrue(:,1)'],...
     [minData.testingLabel_cell{minInd}(:,2)';minData.testingLabelTrue(:,2)'],'color','g')
daspect([1,1,1])
xlim([data.xmin-5,data.xmax+5])
ylim([data.ymin-5,data.ymax+5])
subplot(212);
plot(minData.trainingLabel_cell{minInd}(:,1),minData.trainingLabel_cell{minInd}(:,2),'r*',minData.trainingLabelTrue(:,1),minData.trainingLabelTrue(:,2),'bo','markersize',markersize); legend({'est','true'},'location','northeastoutside')
title(sprintf('[BEST] Train: eps = [%0.2f], C = [%0.2f] || avg L2 error = %0.2f',minEps,minC,min(minData.trainingError)),'fontsize',fontsize)
line([minData.trainingLabel_cell{minInd}(:,1)';minData.trainingLabelTrue(:,1)'],...
     [minData.trainingLabel_cell{minInd}(:,2)';minData.trainingLabelTrue(:,2)'],'color','g')
daspect([1,1,1])
xlim([data.xmin-5,data.xmax+5])
ylim([data.ymin-5,data.ymax+5])
print('-depsc','resultBestCombined')
clear data;