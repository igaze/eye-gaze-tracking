clear all; %clc; close all;
rng(1);
%m = 5;
%n = 5;
%trainingData = 10*rand(m,n);
%trainingLabelTrue = [sum(trainingData,2),sum(trainingData,2).^2];
%testingData = trainingData+rand(size(trainingData));
%testingLabelTrue = trainingLabelTrue;
trainingPercentage = 0.7;
data = load('../data/data_all.mat');
data.designMatrix(floor(size(data.designMatrix,1)/4):end,:) = [];
numTraining = floor(trainingPercentage*size(data.designMatrix,1));
numTesting = size(data.designMatrix,1) - numTraining;
trainingIndices = randsample(size(data.designMatrix,1),numTraining); % random sample
testingIndices = setdiff(1:size(data.designMatrix,1),trainingIndices); % random sample
%trainingIndices = 1:numTraining;
%testingIndices = numTraining+1:size(data.designMatrix,1);

% Dataset Structures
trainingData = data.designMatrix(trainingIndices,:);
testingData = data.designMatrix(testingIndices,:);
trainingLabelTrue = data.allLocations(trainingIndices,:);
testingLabelTrue = data.allLocations(testingIndices,:);


epsilon = 4;
C = 310;


[testingLabel,trainingLabel,avgTestDistError,avgTrainDistError] = SVR(trainingData,trainingLabelTrue,testingData,testingLabelTrue,epsilon,C,'polynomial',1,0,2);
avgTestDistError
avgTrainDistError

figure;
subplot(211);
plot(testingLabel(:,1),testingLabel_cell{ind}(:,2),'r*',testingLabelTrue(:,1),testingLabelTrue(:,2),'bo','markersize',markersize); legend({'est','true'},'location','northeastoutside')
title(['testing err = ' num2str(avgTestDistError)])
line([testingLabel_cell{ind}(:,1)';testingLabelTrue(:,1)'],...
     [testingLabel_cell{ind}(:,2)';testingLabelTrue(:,2)'],'color','g')
daspect([1,1,1])
xlim([data.xmin-5,data.xmax+5])
ylim([data.ymin-5,data.ymax+5])
subplot(212);
plot(trainingLabel_cell{ind}(:,1),trainingLabel_cell{ind}(:,2),'r*',trainingLabelTrue(:,1),trainingLabelTrue(:,2),'bo','markersize',markersize); legend({'est','true'},'location','northeastoutside')
if ind == 1
    title(sprintf('Train: eps = [%0.2f], C = [%0.2f] || avg L2 error = %0.2f',epsMesh(ind),CMesh(ind),trainingError(ind)),'fontsize',fontsize)
else
    title(sprintf('[BEST] Train: eps = [%0.2f], C = [%0.2f] || avg L2 error = %0.2f',epsMesh(ind),CMesh(ind),trainingError(ind)),'fontsize',fontsize)
end
line([trainingLabel_cell{ind}(:,1)';trainingLabelTrue(:,1)'],...
     [trainingLabel_cell{ind}(:,2)';trainingLabelTrue(:,2)'],'color','g')
daspect([1,1,1])
xlim([data.xmin-5,data.xmax+5])
ylim([data.ymin-5,data.ymax+5])