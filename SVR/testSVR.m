clear all; clc; close all; 
% Parameters
chooseBestModelParameterManually = false;
parameterIndex = 6; %6 for 160 feat, 3 for 130feat no eigen %8 for 350 feat 0.4
trialsnum = 1;
trainingPercentage = 0.7;
testingPercentage = 1-trainingPercentage;
fontsize = 15;
markersize = 3;

%% DATA
data = load('../data/data_all.mat');

%% Least squares (with and without) regularization
C = [10:30:70, 200,800,10000];
epsilon = linspace(1e-10,10,20);
[epsMesh,CMesh] = meshgrid(epsilon,C);
% rows are C and cols are epsilon

testingError = zeros(length(C),length(epsilon));
trainingError = zeros(length(C),length(epsilon));
trainingLabel_cell = cell(length(C),length(epsilon));
testingLabel_cell = cell(length(C),length(epsilon));
for i = 1:numel(epsMesh)
    fprintf('model i = %d/%d\n',i,numel(epsMesh))
    for tnum = 1:trialsnum
        rseed = tnum;
        cd ../data
        [trainingData,trainingLabelTrue,testingData,testingLabelTrue] = selectCrossValRand(trainingPercentage,rseed);
        %trainingData(:,end-29:end) = [];
        %testingData(:,end-29:end) = [];
        cd ../SVR
        if i == 1 && tnum == 1
            disp('training size')
            size(trainingData)
            disp('testing size')
            size(testingData)
        end
        [testingLabel,trainingLabel,avgTestDistError,avgTrainDistError] = SVR(trainingData,trainingLabelTrue,testingData,testingLabelTrue,epsMesh(i),CMesh(i));
        testingError(i) = testingError(i) + avgTestDistError;
        trainingError(i) = trainingError(i) + avgTrainDistError;
        % technically this only records the last set of data (should probably be corrected)
        testingLabel_cell{i} = testingLabel;
        trainingLabel_cell{i} = trainingLabel;
    end
    testingError(i) = testingError(i) / trialsnum;
    trainingError(i) = trainingError(i) / trialsnum;
end

%% Parameter selection:
[~,minIndex] = min(testingError(:));
%[minr,minc] = ind2sub([length(C),length(epsilon)],minIndex);
[~,maxIndex] = max(testingError(:));
%[maxr,maxc] = ind2sub([length(C),length(epsilon)],maxIndex);        

% if chooseBestModelParameterManually
%     bestIndex = parameterIndex;
% end

%% Plot results
for ind = [1,minIndex] % [1,worstIndex,bestIndex] 
    [r,c] = ind2sub([length(C),length(epsilon)],ind);
    figure;
    subplot(211);
    plot(testingLabel_cell{ind}(:,1),testingLabel_cell{ind}(:,2),'r*',testingLabelTrue(:,1),testingLabelTrue(:,2),'bo','markersize',markersize); legend({'est','true'},'location','northeastoutside')
    if ind == 1
        title(sprintf('Test: eps = [%0.2f], C = [%0.2f] || avg L2 error = %0.2f',epsMesh(ind),CMesh(ind),testingError(ind)),'fontsize',fontsize)
    else
        title(sprintf('[BEST] Test: eps = [%0.2f], C = [%0.2f] || avg L2 error = %0.2f',epsMesh(ind),CMesh(ind),testingError(ind)),'fontsize',fontsize)
    end
    line([testingLabel_cell{ind}(:,1)';testingLabelTrue(:,1)'],...
         [testingLabel_cell{ind}(:,2)';testingLabelTrue(:,2)'],'color','g')
    daspect([1,1,1])
    xlim([data.xmin-5,data.xmax+5])
    ylim([data.ymin-5,data.ymax+5])
    subplot(212);
    plot(trainingLabel_cell{ind}(:,1),trainingLabel_cell{ind}(:,2),'r*',trainingLabelTrue(:,1),trainingLabelTrue(:,2),'bo','markersize',markersize); legend({'est','true'},'location','northeastoutside')
    if ind == 1
        title(sprintf('Train: eps = [%0.2f], C = [%0.2f] || avg L2 error = %0.2f',epsMesh(ind),CMesh(ind),trainingError(ind)),'fontsize',fontsize)
    else
        title(sprintf('[BEST] Train: eps = [%0.2f], C = [%0.2f] || avg L2 error = %0.2f',epsMesh(ind),CMesh(ind),trainingError(ind)),'fontsize',fontsize)
    end
    line([trainingLabel_cell{ind}(:,1)';trainingLabelTrue(:,1)'],...
         [trainingLabel_cell{ind}(:,2)';trainingLabelTrue(:,2)'],'color','g')
    daspect([1,1,1])
    xlim([data.xmin-5,data.xmax+5])
    ylim([data.ymin-5,data.ymax+5])
    if ind == 1
        print('-depsc','result')
    else
        print('-depsc','resultBest')
    end
end
clear data;
save('SVRModelData.mat')

%{
figure; 
plot(mu,testingError,'bo-',mu,trainingError,'m^-'); hold on;
plot(mu(bestIndex),testingError(bestIndex),'r*');
legend('testingError','trainingError','Best Model Choice')
title('Regularized Linear Regression Tradeoff','fontsize',18)
xlabel('\mu','fontsize',fontsize)
ylabel('Average L2 DistError','fontsize',fontsize)
print -depsc testANDtrainError
%}


hall = figure;
colors = distinguishable_colors(length(C));
plothandles = [];
plotnames = {};
for ci = 1:length(C)
    eps = epsMesh(ci,:);
    [bestTestError,bestInd] = min(testingError(ci,:));
    figure;
    plot(eps,testingError(ci,:),'bo-'); hold on;
    plot(eps(bestInd),bestTestError,'r*');
    legend('testingError','Best Model Choice')
    title(sprintf('SVR model Tradeoff || C = %0.3f',C(ci)),'fontsize',18)
    xlabel('\epsilon','fontsize',fontsize)
    ylabel('Average L2 DistError','fontsize',fontsize)
    print('-depsc',sprintf('testError_C%d_%d',floor(C(ci)),floor(mod(C(ci),1)*100)))
    
    figure(hall);
    h = plot(eps,testingError(ci,:),'o-','color',colors(ci,:)); hold on;
    plot(eps(bestInd),bestTestError,'k*','markersize',markersize);
    plothandles = [plothandles,h];
    plotnames{end+1} = sprintf('C = %0.1f',C(ci));
end
figure(hall);
legend(plothandles,plotnames)
title(sprintf('SVR model Tradeoff'),'fontsize',18)
xlabel('\epsilon','fontsize',fontsize)
ylabel('Average L2 DistError','fontsize',fontsize)
print('-depsc',sprintf('AllTestError'))


