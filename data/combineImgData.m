clear all; clc; close all;
%% Parameters
resizeRatio = 0.1; %set to 1 to ignore
%%

allImgs = [];
allLocations = [];

saveParametersFlag = false;
currentFolders = dir('.');
for i = 1:length(currentFolders) % LOOPS over each day
    if currentFolders(i).name(1) == '.' || ~isdir(currentFolders(i).name)
        continue;
    end
    currFolder = currentFolders(i).name;
    currFolderFiles = dir(fullfile(currFolder,sprintf('data_%s*.mat',currFolder)));
    todaysImgs = [];
    todaysLocations = [];
    for setnum = 1:length(currFolderFiles) % LOOPS over each dataset taken in that dayclear 
        currentDataSet = load(fullfile(currFolder,currFolderFiles(setnum).name));
        oneSetGrayImg = currentDataSet.img_cell;
        oneSetLocations = currentDataSet.locations;
        for imgnum = 1:length(oneSetGrayImg)
            grayImg = imresize(im2double(rgb2gray(oneSetGrayImg{imgnum})),resizeRatio);
            todaysImgs = [todaysImgs;reshape(grayImg,1,numel(grayImg))];
            if ~saveParametersFlag
                xmin = currentDataSet.xmin;
                xmax = currentDataSet.xmax;
                ymin = currentDataSet.ymin;
                ymax = currentDataSet.ymax;
                origrows = size(grayImg,1);
                origcols = size(grayImg,2);
                saveParametersFlag = true;
                sampleImage = grayImg;
            end
        end
        todaysLocations = [todaysLocations;oneSetLocations];
        clear currentDataSet oneSetGrayImg oneSetLocations grayImg
    end
    allImgs = [allImgs;todaysImgs];
    allLocations = [allLocations;todaysLocations];
end

designMatrix = allImgs;
save('data_all.mat','allImgs','allLocations','xmin','xmax','ymin','ymax','origcols','origrows','designMatrix','sampleImage')