function [Train_Data_fin,Train_Label_fin,Test_Data_fin,Test_Label_fin]=selectCrossValRand(percent,r_seed,rescale)
if nargin < 2
    r_seed = 1; %Comment
end
%% Parameters
if nargin < 3
    rescale = 0.17;%0.25
end
%%

load ('data_all.mat');
rng(r_seed)
numTraining=round(percent*size(allImgs,1));
numTesting = size(allImgs,1) - numTraining;
trainingIndices = randsample(size(allImgs,1),numTraining); % random sample
testingIndices = setdiff(1:size(allImgs,1),trainingIndices); % random sample
Train_Data = zeros(numTraining,size(allImgs,2));
Train_Label = zeros(numTraining,2);

Test_Data = zeros(numTesting,size(allImgs,2));
Test_Label = zeros(numTesting,2);

for idx=1:numTraining
    Train_Data(idx,:)=allImgs(trainingIndices(idx),:);
    Train_Label(idx,:)=allLocations(trainingIndices(idx),:);
end

for idx=1:numTesting
    Test_Data(idx,:)=allImgs(testingIndices(idx),:);
    Test_Label(idx,:)=allLocations(testingIndices(idx),:);
end

[Train_Data_fin,Train_Label_fin,Test_Data_fin,Test_Label_fin] = append_eigen_fisher (Train_Data,Train_Label,Test_Data,Test_Label, ...
                                                                                           origrows,origcols,xmax,xmin,ymax,ymin,rescale);
Train_Data_fin(:,end-29:end) = [];
Test_Data_fin(:,end-29:end) = [];
    