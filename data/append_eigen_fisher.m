function [Train_Data_fin,Train_Label_fin,Test_Data_fin,Test_Label_fin] = append_eigen_fisher (Train_Data,Train_Label,Test_Data,Test_Label, ...
                                                                                           origrows,origcols,xmax,xmin,ymax,ymin,rescale)
% Reading images from TrainingData matrix and converting to cell 2D matrices
Num_Images = size(Train_Data,1);
for idx =1:Num_Images
    Im_scale{idx}=reshape(Train_Data(idx,:),origrows,origcols);
    %figure,imshow(Im_scale{idx})
end

%% discretizing into ht*wd classes (generating the dataset for fisher analysis)
ht=3;
wd=4;
num_class=ht*wd;
class_counter=zeros(num_class,1);
[row,col]=size(Im_scale{1});
x_box_range = ceil((xmax-xmin)/wd);
y_box_range = ceil((ymax-ymin)/ht);
for idx=1:Num_Images
    x_bin=floor(Train_Label(idx,1)/x_box_range);
    y_bin=floor(Train_Label(idx,2)/y_box_range);
    class_id=wd*y_bin+x_bin+1;
    class_counter(class_id)=class_counter(class_id)+1;
    Im_train{class_id}(:,:,class_counter(class_id))=Im_scale{idx};
end

%% get the mean eye
[r1,c1]=size(Im_scale{1});
Mean_Im=zeros(r1,c1);
for idx=1:Num_Images
    Mean_Im=Mean_Im+Im_scale{idx};
end
Mean_Im =Mean_Im./Num_Images;

%imshow(Mean_Im,[min(Mean_Im(:)) ,max(Mean_Im(:))])


%% mean eye for each of the classes

for id=1:num_class
    [r,c,num]=size(Im_train{id});
    Class_mean{id}=zeros(r1,c1);
    for index=1:num
        Class_mean{id}=Class_mean{id}+double(Im_train{id}(:,:,index));
    end
    Class_mean{id}=Class_mean{id}./num;
    %figure, imshow(Class_mean{id},[min(Class_mean{id}(:)) max(Class_mean{id}(:))])
end

%%
% get the eigen vectors
S=zeros(r1*c1,Num_Images);
for idx=1:Num_Images
    temp_Im=double(Im_scale{idx})-Mean_Im;
    col_temp=reshape(temp_Im,r1*c1,1);
    S(:,idx)=col_temp;
end
% eigen values and vectors- arrange in descending order
[vec,val]=eig(S'*S);
dg=diag(val);
[eig_val,index]=sort(dg,'descend');
% get the eigen eye
Num_eig_vec=20; %IMPORTANT PARAMETER AS DEFINES NUMBER OF FEATURES
temp_eig_eye=vec(:,index(1:Num_eig_vec)); % choosing top eigen vector (Eigen Image)
eig_eye=S*temp_eig_eye; % Eigen image for SS'

for id=1:Num_eig_vec % normalisation
    eig_eye(:,id)=eig_eye(:,id)/norm(eig_eye(:,id),2);
end
%% Fisher Analysis
% vectorize the mean image of each class
for id=1:num_class
    Class_mean_vec{id}=reshape(Class_mean{id},r1*c1,1);
end
mean_vec=reshape(Mean_Im,r1*c1,1); % vectorize the total mean
%projecting onto lower subspace
mean_vec_pr=eig_eye'*mean_vec;
for id=1:num_class
    Class_mean_vec_pr{id}=eig_eye'*Class_mean_vec{id};
end
dim_Rb=size(mean_vec_pr,1);
Rb=zeros(dim_Rb,dim_Rb);
for id=1:num_class
    [r,c,num]=size(Im_train{id});
    Rb=Rb+num*(Class_mean_vec_pr{id}-mean_vec_pr)*(Class_mean_vec_pr{id}-mean_vec_pr)';
end

Rw=zeros(dim_Rb,dim_Rb);

for id=1:num_class
    [r,c,num]=size(Im_train{id});
    for idx=1:num
        temp_Im=double(Im_train{id}(:,:,idx));
        col_temp=reshape(temp_Im,r1*c1,1);
        %glass_Imvec(:,id)=col_temp;
        col_proj=eig_eye'*col_temp;
        Rw=Rw+(col_proj-Class_mean_vec_pr{id})*(col_proj-Class_mean_vec_pr{id})';
    end
end

[V,D]=eig(Rb,Rw);
num_fish=10; %IMPORTANT PARAMETER AS DEFINES NUMBER OF FEATURES
fisher_eye_vec=eig_eye*V(:,1:num_fish);

%%  Display Eigen eye and Fisher eye
for id=1:Num_eig_vec
    disp_eig_eye(:,:,id)=reshape(eig_eye(:,id),r1,c1);
    temp_Im=disp_eig_eye(:,:,id);
    %figure,imshow(temp_Im,[min(temp_Im(:)) max(temp_Im(:))])
end

for id=1:num_fish % normalisation
    fisher_eye_vec(:,id)=fisher_eye_vec(:,id)/norm(fisher_eye_vec(:,id),2);
    fisher_eye{id}=reshape(fisher_eye_vec(:,id),r1,c1);
    %figure, imshow(fisher_eye{id},[min(fisher_eye{id}(:)) max(fisher_eye{id}(:))])
end
%% Final Training Data - downsampled pixels in image 
for idx=1:Num_Images
    im_temp=imresize(Im_scale{idx},rescale);    
    Train_Data_fin(idx,:)=reshape(im_temp,1,size(im_temp,1)*size(im_temp,2));
end
% Append features to the Training matrix- EIGEN EYE and FISHER EYE
score1=zeros(Num_Images,Num_eig_vec);
score2=zeros(Num_Images,num_fish);

for idx=1:Num_Images
    score1(idx,:)=eig_eye(:,1:Num_eig_vec)'*S(:,idx); % store eigen eyeproduct
    score2(idx,:)=fisher_eye_vec(:,1:num_fish)'*S(:,idx); % store fisher eye product
end
Train_Data_fin=[Train_Data_fin score1 score2];
%figure, imshow(reshape(S(:,1),r1,c1))

Train_Label_fin = Train_Label;
%% Final Testing Data - downsampled pixels in image 
S_Test=zeros(r1*c1,size(Test_Data,1));
for idx=1:size(Test_Data,1)
    Im_test{idx}=reshape(Test_Data(idx,:),origrows,origcols);
    Im_test_mean_rem{idx}=Im_test{idx}-Mean_Im;
    S_Test(:,idx)=Im_test_mean_rem{idx}(:);
    im_t1=imresize(Im_test{idx},rescale);    
    Test_Data_fin(idx,:)=reshape(im_t1,1,size(im_t1,1)*size(im_t1,2));
end
% Append features to the Training matrix- EIGEN EYE and FISHER EYE
score1=zeros(size(Test_Data,1),Num_eig_vec);
score2=zeros(size(Test_Data,1),num_fish);

for idx=1:size(Test_Data,1)
    score1(idx,:)=eig_eye(:,1:Num_eig_vec)'*S_Test(:,idx); % store eigen eyeproduct
    score2(idx,:)=fisher_eye_vec(:,1:num_fish)'*S_Test(:,idx); % store fisher eye product
end
Test_Data_fin=[Test_Data_fin score1 score2];
%figure, imshow(reshape(S(:,1),r1,c1))

Test_Label_fin = Test_Label;
