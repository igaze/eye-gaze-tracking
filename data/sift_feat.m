clc
clear all
%load('masterRefImage.mat');
load('data_all.mat');
for idx=1:size(allImgs,1)
    eye_image{idx}=reshape(allImgs(idx,:),origrows,origcols);   
end
%%
peak_th=1;
edge_th=2;
idx=340;
sig=1;
fil=fspecial('gaussian',[sig+1 sig+1],sig);
im_filtered=imfilter(eye_image{idx},fil,'replicate','conv');
[f,d]=vl_sift(single(im_filtered),'PeakThresh',peak_th,'edgethresh',edge_th);
figure
imagesc(im_filtered);colormap(gray);
hold on
vl_plotframe(f);