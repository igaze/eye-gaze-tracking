function rect=getEyeRect(camera)

image=snapshot(camera);
figure
imagesc(image);
[x,y] = ginput(2)
rect=[x(1) y(1) x(2)-x(1) y(2)-y(1)];
rectangle('position',rect,'EdgeColor','b')

end