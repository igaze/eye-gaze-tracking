function [testingLabel,trainingLabel,avgTestDistError,avgTrainDistError] = leastSquares(trainingData,trainingLabelTrue,testingData,testingLabelTrue,mu,useCVX)
if nargin < 5
    mu = 0;
end
if nargin < 6
    useCVX = false;
end

% If mu = 0, then we do regular linear regularization
if ~useCVX
    if mu == 0
        wx = trainingData\trainingLabelTrue(:,1);
        wy = trainingData\trainingLabelTrue(:,2);
    else
        A = [trainingData;sqrt(mu)*eye(size(trainingData,2))];
        btilde_xlim = [trainingLabelTrue(:,1); zeros(size(trainingData,2),1)];
        btilde_ylim = [trainingLabelTrue(:,2); zeros(size(trainingData,2),1)];

        wx = A\btilde_xlim;
        wy = A\btilde_ylim;
    end
else
    n = size(trainingData,2);
    cvx_begin quiet
        variables wx(n)
        minimize(norm(trainingData*wx-trainingLabelTrue(:,1))+mu*norm(wx,2))
    cvx_end
    cvx_begin quiet
        variables wy(n)
        minimize(norm(trainingData*wy-trainingLabelTrue(:,2))+mu*norm(wy,2))
    cvx_end
end

% Calculate estimation
testingLabel = [testingData*wx,testingData*wy];
trainingLabel = [trainingData*wx,trainingData*wy];

% error
avgTestDistError = 0;
avgTrainDistError = 0;
for i = 1:size(testingLabel,1)
    avgTestDistError = avgTestDistError + norm(testingLabel(i,:)-testingLabelTrue(i,:))/size(testingLabel,1);
end
for i = 1:size(trainingLabel,1)
    avgTrainDistError = avgTrainDistError + norm(trainingLabel(i,:)-trainingLabelTrue(i,:))/size(trainingLabel,1);
end