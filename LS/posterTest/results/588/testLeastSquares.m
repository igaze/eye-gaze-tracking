clear all; clc; close all;
%% Parameters
chooseBestModelParameterManually = false;
parameterIndex = 6; %6 for 160 feat, 3 for 130feat no eigen %8 for 350 feat 0.4
%trialsnum = 1;
trainingPercentage = 0.7;
testingPercentage = 1-trainingPercentage;
fontsize = 15;
markersize = 3;

%% DATA
data = load('../../../data/data_all.mat');
% numTraining = floor(trainingPercentage*size(data.designMatrix,1));
% numTesting = size(data.designMatrix,1) - numTraining;
% trainingIndices = randsample(size(data.allImgs,1),numTraining); % random sample
% testingIndices = setdiff(1:size(data.allImgs,1),trainingIndices); % random sample
% %trainingIndices = 1:numTraining;
% %testingIndices = numTraining+1:size(data.designMatrix,1);
% 
% % Dataset Structures
% trainingData = data.designMatrix(trainingIndices,:);
% testingData = data.designMatrix(testingIndices,:);
% trainingLabelTrue = data.allLocations(trainingIndices,:);
% testingLabelTrue = data.allLocations(testingIndices,:);


%% Least squares (with and without) regularization
mu = linspace(0,0.02,10);
testingError = zeros(1,length(mu));
trainingError = zeros(1,length(mu));
trainingLabel_cell = cell(1,length(mu));
testingLabel_cell = cell(1,length(mu));

cd ../../../data
[trainingData,trainingLabelTrue,testingData,testingLabelTrue] = selectCrossValRand(trainingPercentage,1,0.43);
cd ../LS/posterTest/588
disp('training size')
size(trainingData)
disp('testing size')
size(testingData)
for i = 1:length(mu)
    fprintf('Model i=%d/%d\n',i,numel(mu))
    %for tnum = 1:trialsnum
        %rseed = tnum;
        [testingLabel,trainingLabel,avgTestDistError,avgTrainDistError] = leastSquares(trainingData,trainingLabelTrue,testingData,testingLabelTrue,mu(i),true);
        testingError(i) = testingError(i) + avgTestDistError;
        trainingError(i) = trainingError(i) + avgTrainDistError;
        % technically this only records the last set of data (should probably be corrected)
        testingLabel_cell{i} = testingLabel;
        trainingLabel_cell{i} = trainingLabel;
    %end
    testingError(i) = testingError(i);% / trialsnum;
    trainingError(i) = trainingError(i);% / trialsnum;
end

%% Parameter selection
[~,bestIndex] = min(testingError(2:end));
bestIndex = bestIndex + 1;
if chooseBestModelParameterManually
    bestIndex = parameterIndex;
end

%% Plot results
for ind = [1,bestIndex] % [1,worstIndex,bestIndex] 
    figure;
    subplot(211);
    plot(testingLabel_cell{ind}(:,1),testingLabel_cell{ind}(:,2),'r*',testingLabelTrue(:,1),testingLabelTrue(:,2),'bo','markersize',markersize); legend({'est','true'},'location','northeastoutside')
    if ind == 1
        title(['Test: LS no reg. || avg L2 error = ' num2str(testingError(ind))],'fontsize',fontsize)
    else
        title([sprintf('Test: LS w/ reg. (mu = %f) || avg L2 error = ',mu(bestIndex)) num2str(testingError(ind))],'fontsize',fontsize)
    end
    line([testingLabel_cell{ind}(:,1)';testingLabelTrue(:,1)'],...
         [testingLabel_cell{ind}(:,2)';testingLabelTrue(:,2)'],'color','g')
    daspect([1,1,1])
    xlim([data.xmin-5,data.xmax+5])
    ylim([data.ymin-5,data.ymax+5])
    subplot(212);
    plot(trainingLabel_cell{ind}(:,1),trainingLabel_cell{ind}(:,2),'r*',trainingLabelTrue(:,1),trainingLabelTrue(:,2),'bo','markersize',markersize); legend({'est','true'},'location','northeastoutside')
    if ind == 1
        title(['Train: LS no reg. || avg L2 errr = ' num2str(trainingError(ind))],'fontsize',fontsize)
    else
        title([sprintf('Train: LS w/ reg. (mu = %f) || avg L2 error = ',mu(bestIndex)) num2str(trainingError(ind))],'fontsize',fontsize)
    end
    line([trainingLabel_cell{ind}(:,1)';trainingLabelTrue(:,1)'],...
         [trainingLabel_cell{ind}(:,2)';trainingLabelTrue(:,2)'],'color','g')
    daspect([1,1,1])
    xlim([data.xmin-5,data.xmax+5])
    ylim([data.ymin-5,data.ymax+5])
    if ind == 1
        print -depsc resultLS
    else
        print -depsc resultLSReg
    end
end

figure; 
plot(mu,testingError,'bo-',mu,trainingError,'m^-'); hold on;
plot(mu(bestIndex),testingError(bestIndex),'r*');
legend('testingError','trainingError','Best Model Choice')
title('Regularized Linear Regression Tradeoff','fontsize',18)
xlabel('\mu','fontsize',fontsize)
ylabel('Average L2 DistError','fontsize',fontsize)
print -depsc testANDtrainError

figure; 
plot(mu,testingError,'bo-'); hold on;
plot(mu(bestIndex),testingError(bestIndex),'r*');
legend('testingError','Best Model Choice')
title('Regularized Linear Regression Tradeoff','fontsize',18)
xlabel('\mu','fontsize',fontsize)
ylabel('Average L2 DistError','fontsize',fontsize)
print -depsc testError

clear data;
save('LSData.mat')

% function [trainingData,trainingLabelTrue,testingData,testingLabelTrue] = selectCrossValRand(trainingPercentage,rseed)
% rng(rseed);
% data = load('data/data_all.mat');
% numTraining = floor(trainingPercentage*size(data.designMatrix,1));
% numTesting = size(data.designMatrix,1) - numTraining;
% trainingIndices = randsample(size(data.allImgs,1),numTraining); % random sample
% testingIndices = setdiff(1:size(data.allImgs,1),trainingIndices); % random sample
% %trainingIndices = 1:numTraining;
% %testingIndices = numTraining+1:size(data.designMatrix,1);
% 
% % Dataset Structures
% trainingData = data.designMatrix(trainingIndices,:);
% testingData = data.designMatrix(testingIndices,:);
% trainingLabelTrue = data.allLocations(trainingIndices,:);
% testingLabelTrue = data.allLocations(testingIndices,:);
% end