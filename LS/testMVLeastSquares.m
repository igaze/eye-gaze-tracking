clear all; clc; close all;
% Parameters
chooseBestModelParameterManually = true;
parameterIndex = 6; %6 for 160 feat, 3 for 130feat no eigen %8 for 350 feat 0.4
trialsnum = 1;
trainingPercentage = 0.7;
testingPercentage = 1-trainingPercentage;
fontsize = 15;
markersize = 3;

% DATA
data = load('../data/data_all.mat');
numTraining = floor(trainingPercentage*size(data.designMatrix,1));
numTesting = size(data.designMatrix,1) - numTraining;
trainingIndices = randsample(size(data.allImgs,1),numTraining); % random sample
testingIndices = setdiff(1:size(data.allImgs,1),trainingIndices); % random sample
%trainingIndices = 1:numTraining;
%testingIndices = numTraining+1:size(data.designMatrix,1);

% Dataset Structures
trainingData = data.designMatrix(trainingIndices,:);
testingData = data.designMatrix(testingIndices,:);
trainingLabelTrue = data.allLocations(trainingIndices,:);
testingLabelTrue = data.allLocations(testingIndices,:);

% 
[beta,sigma,e,covb,logl] = mvregress(trainingData,trainingLabelTrue,'maxiter',1000);
%
wx = beta(:,1);
wy = beta(:,2);

% Calculate estimation
testingLabel = [testingData*wx,testingData*wy];
trainingLabel = [trainingData*wx,trainingData*wy];

% error
avgTestDistError = 0;
avgTrainDistError = 0;
for i = 1:size(testingLabel,1)
    avgTestDistError = avgTestDistError + norm(testingLabel(i,:)-testingLabelTrue(i,:))/size(testingLabel,1);
end
for i = 1:size(trainingLabel,1)
    avgTrainDistError = avgTrainDistError + norm(trainingLabel(i,:)-trainingLabelTrue(i,:))/size(trainingLabel,1);
end

avgTestDistError
avgTrainDistError

figure;
subplot(211);
plot(testingLabel(:,1),testingLabel(:,2),'r*',testingLabelTrue(:,1),testingLabelTrue(:,2),'bo','markersize',markersize); legend({'est','true'},'location','northeastoutside')
title(['testing err = ' num2str(avgTestDistError)])
line([testingLabel(:,1)';testingLabelTrue(:,1)'],...
     [testingLabel(:,2)';testingLabelTrue(:,2)'],'color','g')
daspect([1,1,1])
xlim([data.xmin-5,data.xmax+5])
ylim([data.ymin-5,data.ymax+5])
subplot(212);
plot(trainingLabel(:,1),trainingLabel(:,2),'r*',trainingLabelTrue(:,1),trainingLabelTrue(:,2),'bo','markersize',markersize); legend({'est','true'},'location','northeastoutside')
title(['training err = ' num2str(avgTrainDistError)])
line([trainingLabel(:,1)';trainingLabelTrue(:,1)'],...
     [trainingLabel(:,2)';trainingLabelTrue(:,2)'],'color','g')
daspect([1,1,1])
xlim([data.xmin-5,data.xmax+5])
ylim([data.ymin-5,data.ymax+5])
