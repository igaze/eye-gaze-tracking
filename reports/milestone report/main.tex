\documentclass[a4paper]{article}
\usepackage{url}
\usepackage[margin=1in,footskip=0.25in]{geometry}
\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}

\title{iGaze: Dynamic Eye Gaze Tracking and Prediction for Foveated Rendering and Retinal Blur}

\author{Kushagr Gupta,  Suleman Kazi, Terry Kong}
\date{}

\begin{document}
\maketitle

\begin{abstract}
The project deals with determining eye gaze direction and predicting future eye movement sequences using machine learning and artificial intelligence techniques for users wearing head mounted virtual reality displays. This project is a joint project with CS 221 and CS 229. The learning portion of this project is done for the ML project and the prediction portion is done for the AI project. The majority of this report explains the data acquisition, feature extraction, as well as a crude learning approach to justify our choice of prediction model since we did not know Bayesian Networks at the time of the proposal submission. We now have a framework to learn the eye movements as well as a framework to acquire a dataset quickly, and so we now plan to model eye gaze location with Bayesian networks to estimate the posterior probability for prediction.
\end{abstract}

\section{Problem Statement}
In this project we aim to perform infrared eye gaze tracking using standard machine learning techniques (linear regression, SVR) and to predict the future sequence of eye movements with AI techniques based on prior information about the user’s gaze. The ability to predict eye gaze accurately can make a huge impact in Virtual Reality (VR) Headsets since it gives the developer flexibility to enable foveated rendering or retinal blur to enhance content and have an immersive VR experience. Eye gaze prediction also answers the question of what content to adjust when the user is blinking. Both retinal blur and foveated rendering require high accuracy (so that the scene is rendered and blurred at the right locations) and low latency (so that the content is updated perceptually in real-time). Current state of the art is capable of high accuracy, but latency is still a huge bottleneck, especially in foveated rendering. Our approach to eye gaze tracking and prediction will differ from other traditional methods, such as the ones that use Kalman filter for prediction, and will build off of AI techniques such as modeling with Markov Decision Processes.


\section{Dataset Acquisition}
The input data for this problem consists of images of the user's eye looking at different points on a head-mounted VR display. In order to keep costs at a minimum and ease the installation of a camera into the headset we acquired a Google Cardboard VR viewer. However, we were unable to find a suitable camera within our budget to fit into the viewer. In the best interests of time we opted to use a standard off-the-shelf webcam to capture images of the user looking at different points on a computer screen.  The camera is fitted on a stand to the side of the screen, looking at the user's eye off-axis. It is worth mentioning that the camera does not obstruct any part of the designated field-of-view. The user places his head at a fixed distance from the camera and screen. The user is then shown a point at a known location on the screen and asked to look at it. An image of the eye is then captured. This process is repeated for different points spread out over the screen. An advantage of collecting data this way is that we do not have to worry about inadequate lighting . It also allows us to efficiently gather data similar to what what we would have gotten by fitting a camera inside an actual VR headset.  The data collection setup is shown in Fig. \ref{dset}. We have currently collected about $1000$ training images.

In order to facilitate data collection, we automated several parts of the process. The steps involved in acquiring a dataset are described below:

\begin{enumerate}
\item The user places their head on a prefixed location in front of the screen. The camera is positioned to capture the user's eye. If this is the first time the user is using the test setup, the user is asked to look straight at the camera and a reference image is captured (called the master reference Fig. \ref{masterref}). A box is manually drawn around the position of the user's eye in the image, the image is then cropped to this box and saved. This step only needs to be done once.
\item Now the user is ready to take an actual dataset. At the start of each dataset, the user must take another reference image. The master reference obtained in step 1 above is then cross correlated with this reference image to find the location of the user's eye, a box of the same size as the master reference is then drawn around the eye automatically Fig.\ref{refboundbox}. This step helps ensure that any slight variations of user head position between different dataset acquisitions are automatically accounted for. Steps 1 and 2 will not be necessary when using an actual VR headset because the relative position of the camera, the user's eye and the screen remains fixed.
\item The user is then shown a random point on the screen and asked to look at it, each key press by the user captures an image and displays a new point on the screen. Each captured image is cropped based on the bounding box calculated in step 2. Each image is saved along with the location of the corresponding point on the screen. 
\end{enumerate}


\section{Feature Selection}
In the very first implementation of eye gaze tracking, each training image was converted into a vector of pixel values and the pixel values themselves served as features for the algorithm. As a result there were too many features for a relatively lesser number of training images. In order to tackle this issue of prohibitive feature vector dimension, the images were compressed in size and this reduced the number of features being fed to the regression analysis significantly. Different scaling factors for resizing the images were considered and it was observed that downsampling didn't harm the performance and hence for the purpose of computational efficiency we downsampled the images significantly. 

In order to enhance performance we needed features which could discriminate different eye gaze locations to yield superior performance in terms of fitting the data/classification. Among the popular techniques that exist in image processing domain we introduced two sets of features namely the inner product with the eigen-eyes and the inner product with the fisher-eyes \cite{norman} which help in reducing the dimensionality of the problem and enhance variance in principal directions as they work on the idea of Principal Component Analysis. They are explained as follows.
\begin{enumerate}
\item Eigen Eyes : Given the vectorized training images, we remove the mean of the entire set $\mu$ from all the vectors and concatenate them into a matrix, say S, of dimension number of pixels by number of training images. Eigen eyes are essentially the eigen-vectors of the scatter matrix $S*S'$. This gives us the directions in which there is higher variance based on the eigen values.
\item Fisher Eyes : In eigen eyes we consider the entire dataset and discriminate based on that, but we can improve this using the concept of fisher analysis. In this the aim is to maximize between class scatter as provided in eqn. (\ref{between_class}) while minimizing within class scatter, eqn. (\ref{within_class}). For the purpose of eye gaze tracking, we considered a finite set of classes based on the true locations of target point where the eyes are looking at. Then we bin each training image into one of these classes and remove the mean from each class. Finally we calculate the generalized eigen vectors of the problem in eqn. (\ref{eigen}) and call them the fisher eyes.The equations are as follows:
\begin{equation}
\label{between_class}
R_b = \sum_{i=1}^{c} N_i(\mu_i-\mu)(\mu_i-\mu)^H
\end{equation}
\begin{equation}
\label{within_class}
R_w = \sum_{i=1}^{c} \sum_{\tau_l \in Class(i)} (\tau_l-\mu_i)(\tau_l-\mu_i)^H
\end{equation}
\begin{equation}
\label{opt_prob}
W_{opt} = argmax_w (det(WR_BW^H)/det(WR_WW^H))
\end{equation}
\begin{equation}
\label{eigen}
R_Bw_i = \lambda_iR_WW_i
\end{equation}

Using these features we get better discrimination between eye gaze directions, hence improving eye gaze tracking.

While the initial computation of all the eigen-eyes and fisher-eyes may take some time, they will only need to be computed once at the beginning using all the training data.

\end{enumerate}
\section{Preliminary Regression Techniques}
The location that the eye is looking can be characterized by an $x$ and a $y$ location. To simplify the problem, the $x$ direction will be estimated independently of the $y$ location (irrespective of the learning algorithm). A possible modeling choice is to discretize the possible locations that the eye can look in order to change this to a classification problem. However, there is more value to modeling the eye gaze location as a continuum since that allows a finer granularity in the result. We are looking into the following two techniques: regularized linear (least squares) regression and Support Vector regression. For simplicity, we will assume that any learning algorithm is applied to both $x$ and $y$ for the remainder of the report.
\subsection{Linear Regression}
First we will explain linear regression as a precursor to regularized linear regression.
The idea behind linear regression is simple. We assume that the eye gaze location can be modeled as:
$$p = Xw$$
where $p$ is the eye gaze location, $X$ is the design matrix and $w$ is the weight vector.
For linear least squares regression, the objective function is the L2 norm:
$$J(w) = \|p - Xw \|_2^2$$
Amazingly this optimization problem has an analytic solution (if $X$ is skinny)\footnote{It also has an analytic solution if $X$ is fat, but that is not the case here},
$$w_{LS} = X^\dagger p = (X^TX)^{-1}X^Tp$$
where $X^\dagger$ is the pseudoinverse.
One huge assumption is that X must be full rank. For most real data, this is usually not an issue, but with the large number of features that we can have, it is possible to have a (nearly) rank deficient design matrix. If this is the case, we must remove the training data that corresponds to the redundant data.
The results of using linear least squares regression are poor, but that is to be expected from such a crude approach. Figure (\ref{leastsq}) in the appendix is a presentation of the output from linear least squares regression.

\subsection{Regularized Linear Regression}
In any machine learning algorithm, we must take ensure our model is not overfitting the training data.  One solution to this is to regularize the optimization problem in order to drive the elements of the weight vector $w$ to zero. This is equivalent to assuming that $w$ has a Gaussian prior with mean $0$ and a non-negative variance. We will regularize the least squares problem.
In general, the regularized least squares problem can be written as:
\begin{equation}\label{regequation}
J(w)=\|p -Xw\|^2_2+\mu\|g-Fw\|^2_2
\end{equation}
See \cite{regref} for more details.
For our purposes, we will let $F=I_{n}$ where $I_n$ is the $n$-by-$n$ identity matrix and $n$ is the dimension of the feature vector. We will also assume $g = 0$. Thus equation (\ref{regequation}) can be rewritten as 
\begin{equation}\label{regequation}
J(w)=\|p -Xw\|^2_2+\mu\|w\|^2_2
\end{equation}
Again, to no surprise the regularized least squares problem also has a analytic solution:
$$w_{RegLS}=(X^TX+\mu I_n)^{-1}X^Tp$$
The best way to select the model parameter $\mu$ is to plot the optimal trade-off curve which allows us to visualize the effect of changing $\mu$. Observe in Figure (\ref{tradeoffcurve}), points that are lower correspond to lower mean square error\footnote{The vertical axis is in fact the average mean square error, normalized over the number of testing points. This was done in order to get gain an intuition as to the magnitude of the error.}. The reason the trade-off curve begins to increase is the fact that after a certain point, the model begins to overfit. Since we are trying to avoid that we want to pick a $\mu$ that is small. So ideally the optimal choice of the parameter should correspond to the point closest to the bottom left corner. However, sometimes that point does not correspond to the model that overfits the model least, so we have chosen it manually and our choice is shown in Figure (\ref{tradeoffcurve}).

The result of using regularized least squares is shown in Figure (\ref{regularizationresult}). Notice the significant improvement in the testing error (from $6.4295$ to $5.6185$), while the training error suffers only slightly (from $2.6174$ to $2.8707$).\footnote{As a note, reducing the feature vector size by about half has little to no effect on the regularized least squares testing error. The result of reducing the feature vector size by half is shown in Figure (\ref{regularizationresult160}).}

\subsection{Training Error vs. Testing Error (Least Squares)}
We include Figure (\ref{traintesterror}) which includes the training error and testing error to visualize the bias-variance trade-off. As of now we are not certain of the causes of the gap between the training and testing error, but we are certain it should get smaller with more training data, which we plan to gather before the final report.

\subsection{Support Vector Regression}
We are currently working on Support Vector Regression, but the results are not presentable. We have currently set up a convex solver to solve the primal problem:
\begin{equation}\begin{aligned}
\min_{w,b}& \frac{1}{2}\|w \|^2 \\
\text{subject to } & y^{(i)} - w^Tx^{(i)} - b \leq \epsilon \text{ for $i=1,...,m$} \\
& w^Tx^{(i)} - b - y^{(i)} \leq \epsilon \text{ for $i=1,...,m$}
\end{aligned}\end{equation}
However, because of the large number of features, this problem takes a while to solve using CVX\footnote{A package for solving convex problems in MATLAB} because the number of variables is equal to the number of features.
To speed this up, we plan on solving the dual problem. We plan to use the formulation, with slack variables, in \cite{svrref}, which has far fewer variables, since the number of dual variables is equal to the number of training examples. This is helpful because the number of training examples is typically much smaller the number of features.

\section{Prediction and Bayesian Networks}
In order to predict where the user's eye will be looking in the next timestep, given information about the current state, we can use a Bayesian network to model the eye movements. For the problem formulation, we can consider the display to be discretized and divided into cells, each having it's own $(x,y)$ co-ordinate\footnote{The $(x,y)$ coordinate of the center will be used}. When the user is looking at the display, we assume that the eyegaze location is the location of the cell that the user is looking at. Our aim is to predict the next location that the user will look at given some of the previous locations he has been looking at in the previous time steps.

\begin{figure}[h]
    \centering
	\includegraphics[width=0.5\textwidth]{bayesNet.png}
    \caption{Bayesian Network}
    \label{bayesNet} % reference with \ref{bayesNet}
\end{figure} 


Consider the bayesian network where $d_i$ is an $(x,y)$ tuple representing the measured (estimated) eyegaze location, i.e. where on the screen the user is looking at, and $c_i$ is the true eyegaze location. Our goal in this case is to be able to estimate $p(c_i | d_{(i-k)} , d_{(i-(k-1))}, ..., d_{(i-1)})$. Where k is the number of previous $c_i$'s we assume that the next state depends on. Also we will assume that $p(d_i|c_i) \sim  \mathcal{N}(c_i,\sigma^2)$ since there will be uncertainty resulting from the location outputted by the regression algorithm. We will estimate $\sigma^2$ empirically by looking at our average or worst case deviation from the true location. Our strategy would then be to estimate the different probabilities using learning techniques we have studied in class.  The estimation of the posterior probabilities can be sped up by using a particle filter. 

We will consider two models: the Naive Bayes model, which assumes conditional independence between the $c_i$s, and then a more complicated model that does not assume conditional independence.

At this point, we have generated movies of users watching videos which we plan to use with a regression algorithm to generate a dataset for natural eye movement. We will then use this data to update $p(d_i|c_i)$ and $p(c_i|c_{i-1},...,c_{i-k})$ for prediction.

  \begin{thebibliography}{1}

  \bibitem{regref} \url{http://floatium.stanford.edu/ee263/lectures/multiobjective.pdf}

  \bibitem{svrref}  \url{ http://alex.smola.org/papers/2003/SmoSch03b.pdf}

  \bibitem{norman} \url{http://web.stanford.edu/class/ee368/Handouts/Lectures/2015_Autumn/10-EigenImages_16x9.pdf}
  
  \bibitem{don} Dongheng Li; Winfield, D.; Parkhurst, D.J., "Starburst: A hybrid algorithm for video-based eye tracking combining feature-based and model-based approaches," in Computer Vision and Pattern Recognition - Workshops, 2005. CVPR Workshops. IEEE Computer Society Conference on , vol., no., pp.79-79, 25-25 June 2005
  
  \bibitem{doyle}J.Doyle R. Argue, M.Boardman and G.Hickey. Building a low-cost device to track eye movement, 2004.
  \bibitem{rahayfeh}Al-Rahayfeh, A.; Faezipour, M., "Enhanced frame rate for real-time eye tracking using circular hough transform," in Systems, Applications and Technology Conference (LISAT), 2013 IEEE Long Island , vol., no., pp.1-6, 3-3 May 2013
  \bibitem{narco}Narcizo, F.B.; Rangel de Queiroz, J.E.; Gomes, H.M., "Remote Eye Tracking Systems: Technologies and Applications," in Graphics, Patterns and Images Tutorials (SIBGRAPI-T), 2013 26th Conference on , vol., no., pp.15-22, 5-8 Aug. 2013
  \bibitem{giz} \url{http://www.gizmag.com/eye-tracking-oculus-rift/34878/}
  \bibitem{guenter} GUENTER, B., FINCH, M., AND SNYDER, J. 2012. Foveated 3D Graphics. ACM Transactions on Graphics, 31, 6, 164:1–164:10
  \bibitem{rhee} Rhee, JiHye; A Sung, WonJun; Nam, MiYoung; Byun, Hyeran; Rhee, PhillKyu
An Efficient Eye Tracking Using POMDP for Robust Human Computer Interaction
  \bibitem{mahfouz} Mahfouz, S.; Mourad-Chehade, F.; Honeine, P.; Farah, J.; Snoussi, H., "Target Tracking Using Machine Learning and Kalman Filter in Wireless Sensor Networks," inSensors Journal, IEEE , vol.14, no.10, pp.3715-3725, Oct. 2014
  \bibitem{bulling} L. Swirski, A. Bulling, and N. Dodgson. Robust  real-time pupil tracking in highly off-axis images. In Proceedings of the 2012 Symposium on Eye Tracking Research and Applications, ETRA ’12, pages 173–176. ACM, 2012.

  \end{thebibliography}

\newpage
\section{Appendix}


\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{testSetup.jpg}
    \caption{Dataset Acquisition}
    \label{dset} % reference with \ref{dset}
\end{figure} 

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{masterReference.png}
    \caption{Master Reference Image}
    \label{masterref} % reference with \ref{masterref}
\end{figure} 

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{referenceImageWithBoundingBox.png}
    \caption{Reference Image with Bounding Box}
    \label{refboundbox} % reference with \ref{refboundbox}
\end{figure} 


\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{firstFourEigenEyes.png}
    \caption{First Four Eigen Eyes}
    \label{eigenyes} % reference with \ref{eigenyes}
\end{figure} 

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{firstFourFisherEyes.png}
    \caption{First Four Fisher Eyes}
    \label{fishereyes} % reference with \ref{fishereyes}
\end{figure} 


\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{meanEye.png}
    \caption{Mean Eye}
    \label{meaneye} % reference with \ref{meaneye}
\end{figure} 


\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{meanEyeClasses.png}
    \caption{Mean Eye for Each Class}
    \label{meaneyeclasses} % reference with \ref{meaneyeclasses}
\end{figure} 

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{resultLS350feat.eps}
    \caption{Result Of Linear Least Squares (350 Features)}
    \label{leastsq} % reference with \ref{meaneyeclasses}
\end{figure} 

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{testError350feat.eps}
    \caption{Average Testing Error (350 Features) vs. Model Parameter}
    \label{tradeoffcurve} % reference with \ref{meaneyeclasses}
\end{figure} 

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{resultLSReg350feat.eps}
    \caption{Result Of Regularized Linear Least Squares (350 Features)}
    \label{regularizationresult} % reference with \ref{meaneyeclasses}
\end{figure} 

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{testANDtrainError350feat.eps}
    \caption{Training Error vs. Testing Error (350 Features)}
    \label{traintesterror} % reference with \ref{meaneyeclasses}
\end{figure} 

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{resultLSReg160feat.eps}
    \caption{Result Of Regularized Linear Least Squares (160 Features)}
    \label{regularizationresult160} % reference with \ref{meaneyeclasses}
\end{figure} 


\end{document}